# Command Pattern
The Command pattern is pattern in which an object is used to encapsulate all information needed to perform an action or trigger an event at a later time.
This information includes the method name, the object that owns the method and values for the method parameters.

## Problem
We need to create a calculator with possibility to undo operations

## Solution
The **Command Pattern** help us store operations and possibility to undo operations

## Example
Example shows implementation the Command pattern in simple calculator

```ruby
# Operation
class Operation
  attr_accessor :calculator, :command, :number
  def initialize(calculator, command, number)
    @calculator = calculator
    @command = command
    @number = number.to_i
  end

  def calculate
    calculator.execute(command, number)
  end

  def undo_calculate
    undo = { '+' => '-', '-' => '+', '/' => '*', '*' => '/' }
    calculator.execute(undo[command], number)
  end
end

# Calc
class Calc
  attr_accessor :result

  def initialize
    @result = 0
  end

  def execute(command, number)
    case command
    when '+' then self.result += number
    when '-' then self.result -= number
    when '/' then self.result /= number
    when '*' then self.result *= number
    else
      raise('Error! Unknown command!')
    end
    number
  end
end

# User
class User
  attr_accessor :action_number, :actions

  def initialize
    @action_number = 0
    @actions = []
  end

  def calculate_it(operation)
    operation.calculate
    actions[action_number] = operation
    self.action_number += 1
  end

  def undo_calculate
    return if action_number < 0
    self.action_number -= 1
    actions[action_number].undo_calculate
  end
end

user = User.new
calculator = Calc.new
operation = Operation.new(calculator, '+', 5)
user.calculate_it(operation)
operation = Operation.new(calculator, '*', 15)
user.calculate_it(operation)
operation = Operation.new(calculator, '-', 10)
user.calculate_it(operation)
operation = Operation.new(calculator, '/', 5)
user.calculate_it(operation)
user.undo_calculate
user.undo_calculate
puts calculator.result
```

## Tests
```ruby
describe Operation do
  let(:user) { User.new }
  let(:calculator) { Calc.new }

  specify 'should calculate with possibility of undo' do
    operation = Operation.new(calculator, '+', 10)
    user.calculate_it(operation)
    expect(calculator.result).to eq(10)
    operation = Operation.new(calculator, '*', 5)
    user.calculate_it(operation)
    expect(calculator.result).to eq(50)
    operation = Operation.new(calculator, '-', 10)
    user.calculate_it(operation)
    expect(calculator.result).to eq(40)
    operation = Operation.new(calculator, '/', 4)
    user.calculate_it(operation)
    expect(calculator.result).to eq(10)
    user.undo_calculate
    expect(calculator.result).to eq(40)
    user.undo_calculate
    expect(calculator.result).to eq(50)
  end

  specify 'raises an error if unknown command' do
    operation = Operation.new(calculator, '%', 10)
    error_msg = 'Error! Unknown command!'
    expect { user.calculate_it(operation) }.to raise_error error_msg
  end
end
```
