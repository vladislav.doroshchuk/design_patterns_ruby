# Adapter Pattern
The Adapter pattern lets classes work together that couldn't otherwise because of incompatible interfaces

## Problem
We need ensure classes with incompatible interfaces work (realisation properly work method use_tool for the class Screwdriver)

## Solution
The **Adapter Pattern** help us create Adapter for the class with incompatible interfaces

## Example
We create Screwdriver Adapter for using class Screwdriver

```ruby
# Screwdriver
class Screwdriver
  def unscrew!
    puts 'Screwdriver unscrewed the screw'
  end
end

# Tool
class Tool
  def initialize(adapter)
    @adapter = adapter
  end

  def use_tool
    @adapter.use_tool
  end
end

# Screwdriver Adapter
class ScrewdriverAdapter
  def initialize(screwdriver)
    @screwdriver = screwdriver
  end

  def use_tool
    @screwdriver.unscrew!
  end
end

screwdriver = Screwdriver.new
screwdriver_adapter = ScrewdriverAdapter.new(screwdriver)
tool = Tool.new(screwdriver_adapter)

tool.use_tool
```

## Tests
```ruby
describe ScrewdriverAdapter do
  let(:screwdriver) { Screwdriver.new }

  specify 'should create only one Settings instance' do
    screwdriver = Screwdriver.new
    screwdriver_adapter = ScrewdriverAdapter.new(screwdriver)
    tool = Tool.new(screwdriver_adapter)
    expect(tool.use_tool).to eq('Screwdriver unscrewed the screw')
  end
end
```
