# Sights
class Sights
  attr_reader :name
  def initialize(name)
    @name = name
  end

  def visit(visitor)
    visitor.visit(self)
  end
end

# Temple
class Temple < Sights
  def doing
    'prays'
  end
end

# Stadium
class Stadium < Sights
  def doing
    'cheer'
  end
end

# Theater
class Theater < Sights
  def doing
    'applaud'
  end
end

# Journey
class Journey
  def initialize
    @sights = []
  end

  def add_sight(sight)
    @sights << sight
  end

  def visit(visitor)
    @sights.each do |element|
      element.visit(visitor)
    end
  end
end

# SightsVisitor
class SightsVisitor
  def visit(sights)
    puts "Visiting: #{sights.name} and #{sights.doing} there"
  end
end

# SightsPhotoVisitor
class SightsPhotoVisitor
  def visit(sights)
    puts "Visiting with camera: #{sights.name} and #{sights.doing} there"
  end
end

temple = Temple.new('Temple')
stadium = Stadium.new('Stadium')
theater = Theater.new('Theater')

journey = Journey.new
journey.add_sight(temple)
journey.add_sight(stadium)
journey.add_sight(theater)

sights_visitor = SightsVisitor.new
journey.visit(sights_visitor)

sights_visitor = SightsPhotoVisitor.new
journey.visit(sights_visitor)
