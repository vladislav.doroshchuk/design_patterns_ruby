# Prototype Pattern
The Prototype pattern used when the type of objects to create is determined by a prototypical instance, which is cloned to produce new objects

## Problem
We need to create objects determined by a prototypical instance (we will create movies particular genre).

## Solution
The **Prototype Pattern** help us create movie determined by a prototype

## Example
We create movies determined by concrete Movie Prototype

```ruby
# Movie Prototype
class MoviePrototype
  attr_accessor :title, :genre
end

# Horror Movie Prototype
class HorrorMoviePrototype < MoviePrototype
  def initialize
    @genre = 'Horror'
  end
end

# Comedy Movie Prototype
class ComedyMoviePrototype < MoviePrototype
  def initialize
    @genre = 'Comedy'
  end
end

comedy_movie_prototype = ComedyMoviePrototype.new
horror_movie_prototype = HorrorMoviePrototype.new

comedy_movie = comedy_movie_prototype.clone
comedy_movie.title = 'Terminal'
puts comedy_movie.genre
puts comedy_movie.title

horror_movie = horror_movie_prototype.clone
horror_movie.title = 'It'
puts horror_movie.genre
puts horror_movie.title
```

## Tests
```ruby
describe MoviePrototype do
  let(:comedy_movie_prototype) { ComedyMoviePrototype.new }
  let(:horror_movie_prototype) { HorrorMoviePrototype.new }

  specify 'should create comedy movie "Terminal"' do
    comedy_movie = comedy_movie_prototype.clone
    comedy_movie.title = 'Terminal'
    expect(comedy_movie.genre).to eq('Comedy')
    expect(comedy_movie.title).to eq('Terminal')
  end

  specify 'should create horror movie "It"' do
    comedy_movie = horror_movie_prototype.clone
    comedy_movie.title = 'It'
    expect(comedy_movie.genre).to eq('Horror')
    expect(comedy_movie.title).to eq('It')
  end
end
```
