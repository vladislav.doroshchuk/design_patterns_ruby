# Mediator Pattern
With the Mediator pattern communication between objects is encapsulated within a mediator object. 

## Problem
We have to create a box office which will sell tickets only if age of client is more than movies age_limit.

## Solution
The **Mediator Pattern** lets us create class for communication between objects Client and Movie

## Example
Cinema Box Office implement the Mediator pattern.

```ruby
# Movie
class Movie
  attr_accessor :title, :age_limit
  def initialize(title, age_limit)
    @title = title
    @age_limit = age_limit
  end
end

# Client
class Client
  attr_accessor :name, :age
  def initialize(name, age)
    @name = name
    @age = age
  end
end

# Cinema Box Office implement the Mediator pattern
class CinemaBoxOffice
  attr_accessor :cinema_title, :client, :movie
  def initialize(cinema_title, client, movie)
    @cinema_title = cinema_title
    @client = client
    @movie = movie
  end

  def buy_a_ticket
    if movie.age_limit <= client.age
      return "Client #{client.name} bought a ticket on movie #{movie.title}"
    end
    "Buying a ticket to #{client.name} is forbidden (restriction on age)"
  end
end

movie_avengers = Movie.new('Avengers: Infinity War', 12)
movie_it = Movie.new('It', 18)

client_bill = Client.new('Bill', 10)
client_andry = Client.new('Andry', 16)
client_john = Client.new('John', 25)

cinema_box_office = CinemaBoxOffice.new('Portal', client_bill, movie_avengers)
puts cinema_box_office.buy_a_ticket

cinema_box_office = CinemaBoxOffice.new('Portal', client_andry, movie_avengers)
puts cinema_box_office.buy_a_ticket

cinema_box_office = CinemaBoxOffice.new('Portal', client_john, movie_avengers)
puts cinema_box_office.buy_a_ticket

cinema_box_office = CinemaBoxOffice.new('Portal', client_bill, movie_it)
puts cinema_box_office.buy_a_ticket

cinema_box_office = CinemaBoxOffice.new('Portal', client_andry, movie_it)
puts cinema_box_office.buy_a_ticket

cinema_box_office = CinemaBoxOffice.new('Portal', client_john, movie_it)
puts cinema_box_office.buy_a_ticket
```

## Tests
```ruby
describe Movie do
  let(:avengers) { Movie.new('Avengers: Infinity War', 12) }
  let(:movie_it) { Movie.new('It', 18) }
  let(:client_bill) { Client.new('Bill', 10) }
  let(:client_andry) { Client.new('Andry', 16) }
  let(:client_john) { Client.new('John', 25) }

  specify 'should return age limit for every genre' do
    cinema_box_office = CinemaBoxOffice.new('Portal', client_bill, avengers)
    msg = 'Buying a ticket to Bill is forbidden (restriction on age)'
    expect(cinema_box_office.buy_a_ticket).to eq(msg)

    cinema_box_office = CinemaBoxOffice.new('Portal', client_andry, avengers)
    msg = 'Client Andry bought a ticket on movie Avengers: Infinity War'
    expect(cinema_box_office.buy_a_ticket).to eq(msg)

    cinema_box_office = CinemaBoxOffice.new('Portal', client_john, avengers)
    msg = 'Client John bought a ticket on movie Avengers: Infinity War'
    expect(cinema_box_office.buy_a_ticket).to eq(msg)

    cinema_box_office = CinemaBoxOffice.new('Portal', client_bill, movie_it)
    msg = 'Buying a ticket to Bill is forbidden (restriction on age)'
    expect(cinema_box_office.buy_a_ticket).to eq(msg)

    cinema_box_office = CinemaBoxOffice.new('Portal', client_andry, movie_it)
    msg = 'Buying a ticket to Andry is forbidden (restriction on age)'
    expect(cinema_box_office.buy_a_ticket).to eq(msg)

    cinema_box_office = CinemaBoxOffice.new('Portal', client_john, movie_it)
    msg = 'Client John bought a ticket on movie It'
    expect(cinema_box_office.buy_a_ticket).to eq(msg)
  end
end
```
