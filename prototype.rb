# Movie Prototype
class MoviePrototype
  attr_accessor :title, :genre
end

# Horror Movie Prototype
class HorrorMoviePrototype < MoviePrototype
  def initialize
    @genre = 'Horror'
  end
end

# Comedy Movie Prototype
class ComedyMoviePrototype < MoviePrototype
  def initialize
    @genre = 'Comedy'
  end
end

comedy_movie_prototype = ComedyMoviePrototype.new
horror_movie_prototype = HorrorMoviePrototype.new

comedy_movie = comedy_movie_prototype.clone
comedy_movie.title = 'Terminal'
puts comedy_movie.genre
puts comedy_movie.title

horror_movie = horror_movie_prototype.clone
horror_movie.title = 'It'
puts horror_movie.genre
puts horror_movie.title
