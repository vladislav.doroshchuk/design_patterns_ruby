# Factory Method Pattern
Factory Method Pattern is a creational pattern that uses factory methods to deal with the problem of creating objects without having to specify the exact class of the object that will be created.

## Problem
We need to create objects depending on configuration.

## Solution

The **Factory Method** pattern provide us creating objects without having to specify the exact class of the object that will be created. 

## Example
Depend on configuration create and render a Windows looking button and bind a native OS click event or HTML representation of a button and bind a web browser click event.

```ruby
# Button
class Button
  attr_accessor :color
  def initialize(color = 'green')
    @color = color
  end
end

# Windows Button
class WindowsButton < Button
  def render_button
    puts "Get Windows Button color #{color}"
  end

  def on_click
    puts 'Click Windows Button'
  end
end

# HTML Button
class HTMLButton < Button
  def render_button
    puts "Get HTML Button color #{color}"
  end

  def on_click
    puts 'Click HTML Button'
  end
end

# Dialog
class Dialog
  def render_window(button_color)
    button_ok = create_button(button_color)
    button_ok.render_button
    button_ok.on_click
  end
end

# Windows Dialog
class WindowsDialog < Dialog
  def create_button(button_color)
    WindowsButton.new(button_color)
  end
end

# Web Dialog
class WebDialog < Dialog
  def create_button(button_color)
    HTMLButton.new(button_color)
  end
end

# Client Application
class ClientApplication
  attr_accessor :dialog, :config
  def initialize
    sys_config = random_config
    case sys_config
    when 'windows'
      @dialog = WindowsDialog.new
    when 'web'
      @dialog = WebDialog.new
    else
      raise('Error! Unknown operating system.')
    end
  end

  def random_config
    %w[windows web].sample
  end

  def main(button_color)
    dialog.render_window(button_color)
  end
end
```

## Tests
```ruby
describe Button do
  let(:button) { Button.new('red') }
  specify 'sutton color should be red' do
    expect(button.color).to eq('red')
    button_green = Button.new('green')
    expect(button_green.color).to eq('green')
  end
end

describe ClientApplication do
  specify 'should render red Windows Button' do
    app = ClientApplication.new('windows')
    button = app.main('red')
    expect(button).to be_an_instance_of(WindowsButton)
    expect(button.color).to eq('red')
    expect(button.type).to eq('Windows')
  end

  specify 'should render blue HTML Button' do
    app_html = ClientApplication.new('html')
    button_html = app_html.main('blue')
    expect(button_html).to be_an_instance_of(HTMLButton)
    expect(button_html.color).to eq('blue')
    expect(button_html.type).to eq('HTML')
  end

  specify 'raises an error if unknown operating system' do
    error_msg = 'Error! Unknown operating system!'
    expect { ClientApplication.new('test') }.to raise_error error_msg
  end
end
```
