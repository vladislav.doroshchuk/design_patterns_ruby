# Abstract Factory Pattern
Abstract Factory Pattern is a creational pattern that provide an interface for creating families of related objects, without specifying concrete classes.

## Problem
We need to create families of related objects (different buttons and checkboxes for different OS).

## Solution

The **Abstract Factory Pattern** pattern provide us creating families of related objects. 

## Example
Depend on configuration create a Windows looking button and checkbox or Mac looking button and checkbox.

```ruby
# Button
class Button
end

# WinButton
class WinButton < Button
  def paint
    puts 'WinButton'
  end
end

# MacButton
class MacButton < Button
  def paint
    puts 'MacButton'
  end
end

# Checkbox
class Checkbox
end

# WinCheckbox
class WinCheckbox < Checkbox
  def paint
    puts 'WinCheckbox'
  end
end

# Mac Checkbox
class MacCheckbox < Checkbox
  def paint
    puts 'MacCheckbox'
  end
end

# GUIFactory
class GUIFactory
end

# Win Factory
class WinFactory < GUIFactory
  def create_button
    WinButton.new
  end

  def create_checkbox
    WinCheckbox.new
  end
end

# Mac Factory
class MacFactory < GUIFactory
  def create_button
    MacButton.new
  end

  def create_checkbox
    MacCheckbox.new
  end
end

# Application
class Application
  attr_accessor :button, :factory, :checkbox

  def initialize(factory)
    @factory = factory
  end

  def create_ui
    self.button = factory.create_button
    self.checkbox = factory.create_checkbox
  end

  def paint
    button.paint
    checkbox.paint
  end
end

# Application Configurator
class ApplicationConfigurator
  attr_accessor :app, :factory

  def initialize
    sys_config = random_config
    case sys_config
    when 'windows'
      @factory = WinFactory.new
    when 'web'
      @factory = MacFactory.new
    else
      raise('Error! Unknown operating system.')
    end
  end

  def random_config
    %w[windows web].sample
  end

  def main
    Application.new(factory)
  end
end
```

## Tests
```ruby
describe ApplicationConfigurator do
  specify 'should render Windows Button and CheckBox' do
    app_config = ApplicationConfigurator.new('windows')
    app = app_config.main
    app.create_ui
    expect(app.render).to eq('button: WinButton, checkbox: WinCheckbox')
  end

  specify 'should render Mac Button and CheckBox' do
    app_config = ApplicationConfigurator.new('mac')
    app = app_config.main
    app.create_ui
    expect(app.render).to eq('button: MacButton, checkbox: MacCheckbox')
  end

  specify 'raises an error if unknown operating system' do
    error_msg = 'Error! Unknown operating system!'
    expect { ApplicationConfigurator.new('test') }.to raise_error error_msg
  end
end
```
