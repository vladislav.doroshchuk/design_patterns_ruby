# Phone
class Phone
  attr_reader :status, :status_msg
  def next_status
    @status = @status.next
  end
end

# Phone Turned Off
class PhoneTurnedOff < Phone
  def initialize
    @status = self
    @status_msg = 'Phone is turned off'
  end

  def next
    PhoneTurnedOn.new
  end
end

# Phone Turned On
class PhoneTurnedOn < Phone
  def initialize
    @status_msg = 'Phone is turned on'
  end

  def next
    PhoneClickStart.new
  end
end

# Phone Click Start
class PhoneClickStart < Phone
  def initialize
    @status_msg = 'Phone require click to start button'
  end

  def next
    PhoneReadyToWork.new
  end
end

# Phone Ready To Work
class PhoneReadyToWork < Phone
  def initialize
    @status_msg = 'Phone is ready to work'
  end

  def next
    PhoneTurnedOff.new
  end
end

phone = PhoneTurnedOff.new
5.times do
  puts phone.status.status_msg
  phone.next_status
end
