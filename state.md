# State Pattern
The State Pattern is pattern that implements a state machine in an object-oriented way. 
With the state pattern, a state machine is implemented by implementing each individual state as a derived class of the state pattern interface, and implementing state transitions by invoking methods defined by the pattern's superclass.

## Problem
We have to create a several different states of phone

## Solution
The **State Pattern** lets to the phone to alter its behavior when its internal state changes

## Example
This example implement the State pattern and it`s look like object changed its class

```ruby
# Phone
class Phone
  attr_reader :status, :status_msg
  def next_status
    @status = @status.next
  end
end

# Phone Turned Off
class PhoneTurnedOff < Phone
  def initialize
    @status = self
    @status_msg = 'Phone is turned off'
  end

  def next
    PhoneTurnedOn.new
  end
end

# Phone Turned On
class PhoneTurnedOn < Phone
  def initialize
    @status_msg = 'Phone is turned on'
  end

  def next
    PhoneClickStart.new
  end
end

# Phone Click Start
class PhoneClickStart < Phone
  def initialize
    @status_msg = 'Phone require click to start button'
  end

  def next
    PhoneReadyToWork.new
  end
end

# Phone Ready To Work
class PhoneReadyToWork < Phone
  def initialize
    @status_msg = 'Phone is ready to work'
  end

  def next
    PhoneTurnedOff.new
  end
end

phone = PhoneTurnedOff.new
5.times do
  puts phone.status.status_msg
  phone.next_status
end

```

## Tests
```ruby
describe PhoneTurnedOff do
  let(:phone) { PhoneTurnedOff.new }
  let(:msg) do
    [
      'Phone is turned off',
      'Phone is turned on',
      'Phone require click to start button',
      'Phone is ready to work',
      'Phone is turned off'
    ]
  end

  specify 'should change states of phone' do
    5.times do |index|
      expect(phone.status.status_msg).to eq(msg[index])
      phone.next_status
    end
  end
end
```
