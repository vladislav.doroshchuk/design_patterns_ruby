require 'pry'
# Remote
class Remote
  attr_accessor :device

  def initialize(device)
    @device = device
  end

  def toggle_power
    if device.device_is_enabled?
      device.device_disable
    else
      device.device_enable
    end
  end

  def volume_down
    device.volume -= 10
  end

  def volume_up
    device.volume += 10
  end

  def channel_down
    device.channel -= 1
  end

  def channel_up
    device.channel += 1
  end
end

# AdvancedRemote
class AdvancedRemote < Remote
  def mute
    device.volume = 0
    puts "#{device.device_type} sound off"
  end
end

# Device
class Device
  attr_accessor :enable, :device_type, :volume, :channel

  def initialize(enable, volume, channel)
    self.enable = enable
    self.volume = volume
    self.channel = channel
  end

  def device_is_enabled?
    self.enable
  end

  def device_enable
    self.enable = true
    puts "#{device_type} is enabled"
  end

  def device_disable
    self.enable = false
    puts "#{device_type} is disable"
  end
end

# Tvset
class Tvset < Device
  def initialize(enable = true, volume = 20, channel = 10)
    self.device_type = 'Tvset'
    super(enable, volume, channel)
  end
end

# Radio
class Radio < Device
  def initialize(enable = false, volume = 30, channel = 1)
    self.device_type = 'Radio'
    super(enable, volume, channel)
  end
end

tv = Tvset.new(false, 20, 10)
remote = Remote.new(tv)
remote.toggle_power
remote.toggle_power
remote.toggle_power

radio = Radio.new
remote = AdvancedRemote.new(radio)
remote.mute
remote.toggle_power
remote.toggle_power
