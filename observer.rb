# Spectator
class Spectator
  attr_accessor :name

  def initialize(name)
    @name = name
  end

  def inform(cinema)
    "Spectator #{name} is informed about new movie: #{cinema.movie}"
  end
end

# Cinema
class Cinema
  attr_accessor :spectators, :movie
  def initialize
    @spectators = []
    @movie = ''
  end

  def add_spectator(spectator)
    spectators << spectator
  end

  def remove_spectator(spectator_id)
    spectators.delete_at spectator_id
  end

  def notify
    inform_log = []
    spectators.each do |spectator|
      inform_log << spectator.inform(self)
    end
    inform_log
  end
end

cinema = Cinema.new
cinema.add_spectator(Spectator.new('Jon'))
cinema.add_spectator(Spectator.new('Jessy'))
cinema.add_spectator(Spectator.new('Tom'))

cinema.movie = 'The Crimes of Grindelwald'
cinema.remove_spectator(0)

cinema.notify.each_with_index do |value, index|
  puts "index: #{index}, value: #{value}"
end
