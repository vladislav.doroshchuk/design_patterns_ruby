# Design Patterns (Ruby)
Design patterns are formalized best practices that the programmer can use to solve common problems when designing an application or system.

## Creational Design Patterns 
Creational design patterns are design patterns that deal with object creation mechanisms, trying to create objects in a manner suitable to the situation

* [Factory Method](factory_method.md): The Factory Method pattern uses factory methods to deal with the problem of creating objects without having to specify the exact class of the object that will be created
* [Abstract Factory](abstract_factory.md): The purpose of the Abstract Factory is to provide an interface for creating families of related objects, without specifying concrete classes.
* [Builder](builder.md): The Builder pattern separates the construction of a complex object from its representation so that the same construction process can create different representations.
* [Prototype](prototype.md): The Prototype pattern used when the type of objects to create is determined by a prototypical instance, which is cloned to produce new objects
* [Singleton](singleton.md): The Singleton pattern ensures that a class has only one instance and provides a global point of access to that instance

## Structural Design Patterns
Structural design patterns are design patterns that ease the design by identifying a simple way to realize relationships between entities.

* [Adapter](adapter.md): The Adapter pattern lets classes work together that couldn't otherwise because of incompatible interfaces.
* [Bridge](bridge.md): The Bridge pattern decouple an abstraction from its implementation so that the two can vary independently.
* [Composite](composite.md): The Composite pattern intent of a composite is to "compose" objects into tree structures to represent part-whole hierarchies. Implementing the composite pattern lets clients treat individual objects and compositions uniformly.
* [Decorator](decorator.md): The Decorator pattern allows behavior to be added to an individual object, either statically or dynamically, without affecting the behavior of other objects from the same class.
* [Facade](facade.md): The Facade pattern lets provide a simplified interface to a complex system of classes, library or framework.
* [Flyweight](flyweight.md): The Flyweight pattern lets fit more objects into the available amount of RAM by sharing common parts of object state among multiple objects, instead of keeping it in each object.
* [Proxy](proxy.md): The Proxy pattern provide a substitute or placeholder for another object to control access to it.

## Behavioral Design Patterns
Behavioral design patterns are design patterns that identify common communication patterns between objects and realize these patterns.

* [Chain of Responsibility](chain_of_responsibility.md): The Chain of Responsibility pattern lets you avoid coupling the sender of a request to its receiver by giving more than one object a chance to handle the request. Chain the receiving objects and pass the request along the chain until an object handles it.
* [Command](command.md): The Command pattern is pattern in which an object is used to encapsulate all information needed to perform an action or trigger an event at a later time. This information includes the method name, the object that owns the method and values for the method parameters.
* [Iterator](iterator.md): The Iterator pattern is pattern in which an iterator is used to traverse a container and access the container's elements.
* [Mediator](mediator.md): With the Mediator pattern communication between objects is encapsulated within a mediator object. 
* [Memento](memento.md):  The Memento pattern lets you capture the object's internal state without exposing its internal structure, so that the object can be returned to this state later. 
* [Observer](observer.md):  The Observer pattern is pattern pattern in which an object, called the subject, maintains a list of its dependents, called observers, and notifies them automatically of any state changes, usually by calling one of their methods.
* [State](state.md):  The State pattern is pattern that implements a state machine in an object-oriented way. With the state pattern, a state machine is implemented by implementing each individual state as a derived class of the state pattern interface, and implementing state transitions by invoking methods defined by the pattern's superclass.
* [Strategy](strategy.md):  The Strategy pattern (also known as the policy pattern) is a pattern that enables selecting an algorithm at runtime. Instead of implementing a single algorithm directly, code receives run-time instructions as to which in a family of algorithms to use.
* [Template](template.md):  The Template pattern is a pattern that defines the program skeleton of an algorithm in an operation, deferring some steps to subclasses. It lets one redefine certain steps of an algorithm without changing the algorithm's structure.
* [Visitor](visitor.md):  The Visitor pattern is a way of separating an algorithm from an object structure on which it operates. A practical result of this separation is the ability to add new operations to existent object structures without modifying the structures.

## Contributing

Contributions are welcome!
