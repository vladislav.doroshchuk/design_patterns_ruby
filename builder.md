# Builder Pattern
The Builder Pattern separates the construction of a complex object from its representation so that the same construction process can create different representations.

## Problem
We need to separates the construction of a complex object from its representation (same construction process can create different products, in this case - different pizzas).

## Solution
The **Builder Pattern** help us separates the construction of objects from representation. 

## Example
We create the pizzas using concrete builder depend on clients orders.

```ruby
# Pizza
class Pizza
  attr_accessor :dough, :sauce, :topping
end

# Pizza Builder
class PizzaBuilder
  attr_accessor :pizza
  def create_new_pizza_product
    @pizza = Pizza.new
  end
end

# Concrete Pizza Builder RoastedPorkPizzaBuilder
class RoastedPorkPizzaBuilder < PizzaBuilder
  def build_dough
    pizza.dough = 'cross'
  end

  def build_sauce
    pizza.sauce = 'tomato'
  end

  def build_topping
    pizza.topping = 'roasted pork'
  end
end

# Concrete Pizza Builder PepperoniPizzaBuilder
class PepperoniPizzaBuilder < PizzaBuilder
  def build_dough
    pizza.dough = 'pan baked'
  end

  def build_sauce
    pizza.sauce = 'chili'
  end

  def build_topping
    pizza.topping = 'pepperoni+salami'
  end
end

# Waiter
class Waiter
  attr_accessor :pizza_builder

  def pizza
    pizza_builder.pizza
  end

  def construct_pizza
    pizza_builder.create_new_pizza_product
    pizza_builder.build_dough
    pizza_builder.build_sauce
    pizza_builder.build_topping
  end
end

# A customer ordering a pizza.
class PizzaOrdering
  def order
    pizzas = []
    waiter = Waiter.new
    roasted_pork_pizza_builder = RoastedPorkPizzaBuilder.new
    pepperoni_pizza_builder = PepperoniPizzaBuilder.new
    waiter.pizza_builder = roasted_pork_pizza_builder
    waiter.construct_pizza
    pizzas << waiter.pizza
    waiter.pizza_builder = pepperoni_pizza_builder
    waiter.construct_pizza
    pizzas << waiter.pizza
  end
end
```

## Tests
```ruby
describe PizzaOrdering do
  let(:pizza_order) { PizzaOrdering.new }

  specify 'should return array with two Pizza instance' do
    pizza_order.order
    expect(pizza_order.order.size).to eq(2)
    pizza_order.order.each do |pizza|
      expect(pizza).to be_an_instance_of(Pizza)
    end
    expect(pizza_order.order[0].topping).to eq('roasted pork')
    expect(pizza_order.order[0].dough).to eq('cross')
    expect(pizza_order.order[0].sauce).to eq('tomato')

    expect(pizza_order.order[1].topping).to eq('pepperoni+salami')
    expect(pizza_order.order[1].dough).to eq('pan baked')
    expect(pizza_order.order[1].sauce).to eq('chili')
  end
end
```
