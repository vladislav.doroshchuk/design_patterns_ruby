# Flyweight Pattern
The Flyweight pattern lets fit more objects into the available amount of RAM by sharing common parts of object state among multiple objects, instead of keeping it in each object.

## Problem
We need to storing the same data in several flyweight objects instead of in multiple objects.

## Solution
The **Flyweight Pattern** help us sharing common parts of object state among multiple objects, instead of keeping it in each object. 

## Example
We create MovieOptionsFactory class which implements Flyweight pattern

```ruby
# Movie Options
class MovieOptions
  attr_accessor :genre, :year, :country
  def initialize(genre, year, country)
    self.genre = genre
    self.year = year
    self.country = country
  end
end

# Movie Options Factory (implement Flyweight pattern)
class MovieOptionsFactory
  attr_accessor :movie_options_arr
  def initialize
    self.movie_options_arr = []
  end

  def find_movie_option(genre, year, country)
    movie_options_arr.each do |movie_options|
      if  movie_options && movie_options.genre == genre &&
          movie_options.year == year &&
          movie_options.country == country
        return movie_options
      end
    end
    movie_option = MovieOptions.new(genre, year, country)
    movie_options_arr.push(movie_option)
    movie_option
  end
end

# Movie
class Movie
  attr_accessor :name, :movie_option
  def initialize(name, movie_option)
    self.name = name
    self.movie_option = movie_option
  end

  def show_movie
    puts "Movie \"#{name}\":
    - genre: #{movie_option.genre},
    - year: #{movie_option.year},
    - country: #{movie_option.country}"
  end
end

# Video Collection
class VideoCollection
  attr_accessor :movies, :movie_option_factory
  def initialize
    self.movies = []
    self.movie_option_factory = MovieOptionsFactory.new
  end

  def add_movie(name, genre, year, country)
    movie_option = movie_option_factory.find_movie_option(genre, year, country)
    movie = Movie.new(name, movie_option)
    movies.push(movie)
  end

  def all_movies
    movies.map(&:show_movie)
  end
end

video_collection = VideoCollection.new
video_collection.add_movie(
  'Avengers: Infinity War',
  'Fantasy/Science fiction film',
  2018,
  'USA'
)
video_collection.add_movie(
  'Black Panther',
  'Fantasy/Science fiction film',
  2018,
  'USA'
)
video_collection.add_movie(
  'Tomb Raider',
  'Fantasy/Action',
  2018,
  'USA'
)
video_collection.add_movie(
  'Kill Bill: Volume 1',
  'Crime film/Thriller',
  2003,
  'USA'
)
video_collection.add_movie(
  'Kill Bill: Volume 2',
  'Crime film/Thriller',
  2004,
  'country'
)
video_collection.all_movies
```

## Tests
```ruby
describe VideoCollection do
  let(:video_collection) { VideoCollection.new }

  specify 'should deliver first part of messages' do
    video_collection.add_movie('Avengers: Infinity War', 'Fantasy', 2018, 'USA')
    video_collection.add_movie('Black Panther', 'Fantasy', 2018, 'USA')
    video_collection.add_movie('Tomb Raider', 'Fantasy/Action', 2018, 'USA')
    movies = video_collection.all_movies
    expect(movies.size).to eq(3)
    expect(movies[0]['name']).to eq('Avengers: Infinity War')
    expect(movies[1]['name']).to eq('Black Panther')
    expect(movies[2]['name']).to eq('Tomb Raider')

    expect(movies[0]['genre']).to eq('Fantasy')
    expect(movies[1]['genre']).to eq('Fantasy')
    expect(movies[2]['genre']).to eq('Fantasy/Action')

    expect(movies[0]['movie_option']).to eq(movies[1]['movie_option'])
    expect(movies[0]['movie_option']).not_to eq(movies[2]['movie_option'])
  end
end
```
