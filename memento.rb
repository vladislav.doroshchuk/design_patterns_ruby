# Link
class Link
  attr_accessor :link

  def initialize(link)
    @link = link
  end
end

# Browser
class Browser
  attr_accessor :link

  def follow_the_link(link)
    @link = link
  end

  def save_link_to_history
    Link.new(link)
  end

  def link_from_history(history)
    @link = history.link
  end
end

# History
class History
  attr_accessor :history_array, :current_index

  def initialize
    @history_array = []
    @current_index = 0
  end

  def add(history)
    self.current_index = history_array.length
    history_array.push(history)
  end

  def history_by_index(index)
    history_array[index]
  end

  def undo
    raise('Error! Previous link does not exist!') if current_index <= 0
    self.current_index -= 1
    history_array[self.current_index]
  end
end

browser = Browser.new
history = History.new

browser.follow_the_link('https://stackoverflow.com')
history.add(browser.save_link_to_history)
browser.follow_the_link('https://github.com')
history.add(browser.save_link_to_history)
browser.follow_the_link('https://www.google.com')
history.add(browser.save_link_to_history)
browser.follow_the_link('https://slack.com')
history.add(browser.save_link_to_history)

puts 'Current link: ' + browser.link
browser.link_from_history(history.undo)
puts 'First undo: ' + browser.link

browser.link_from_history(history.undo)
puts 'Second undo:  ' + browser.link

browser.link_from_history(history.undo)
puts 'Second undo:  ' + browser.link

browser.link_from_history(history.history_by_index(0))
puts 'First link: ' + browser.link

browser.link_from_history(history.history_by_index(3))
puts 'Last link: ' + browser.link
