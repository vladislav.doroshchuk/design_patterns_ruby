# Chain of Responsibility Pattern
The Chain of Responsibility pattern lets you avoid coupling the sender of a request to its receiver by giving more than one object a chance to handle the request. Chain the receiving objects and pass the request along the chain until an object handles it.

## Problem
We need to get age_limit for movies depend on movie genre.

## Solution
The **Chain of Responsibility Pattern** help us create chain the receiving objects and pass the request along the chain until an object handles it.

## Example
The class Movie implement the Chain of Responsibility pattern

```ruby
# Movie Information
class MovieInformation
  attr_accessor :title, :genre, :age_limit

  def initialize(title, genre)
    self.title = title
    self.genre = genre
  end

  def message
    "#{genre} #{title} has age limit #{age_limit}+"
  end
end

# Horror Movie
class HorrorMovie
  attr_accessor :next_chain
  def movie_age_limit(movie)
    if movie.genre == 'Horror'
      movie.age_limit = 18
    else
      next_chain.movie_age_limit(movie)
    end
  end

  def next_in_chain(next_chain)
    self.next_chain = next_chain
  end
end

# Thriller Movie
class ThrillerMovie
  attr_accessor :next_chain
  def movie_age_limit(movie)
    if movie.genre == 'Thriller'
      movie.age_limit = 16
    else
      next_chain.movie_age_limit(movie)
    end
  end

  def next_in_chain(next_chain)
    self.next_chain = next_chain
  end
end

# Comedy Movie
class ComedyMovie
  attr_accessor :next_chain
  def movie_age_limit(movie)
    if movie.genre == 'Comedy'
      movie.age_limit = 6
    else
      next_chain.movie_age_limit(movie)
    end
  end

  def next_in_chain(next_chain)
    self.next_chain = next_chain
  end
end

# Tale Movie
class TaleMovie
  attr_accessor :next_chain
  def movie_age_limit(movie)
    raise('Only for Horror, Thriller, Comedy and Tale') if movie.genre != 'Tale'
    movie.age_limit = 0
  end
end

# Movie
class Movie
  attr_accessor :title, :genre

  def initialize(title, genre)
    self.title = title
    self.genre = genre
  end

  def chain
    horror = HorrorMovie.new
    thriller = ThrillerMovie.new
    comedy = ComedyMovie.new
    tale = TaleMovie.new
    horror.next_in_chain(thriller)
    thriller.next_in_chain(comedy)
    comedy.next_in_chain(tale)
    movie_information = MovieInformation.new(title, genre)
    horror.movie_age_limit(movie_information)
    movie_information.message
  end
end

movie = Movie.new('It', 'Horror')
puts movie.chain

movie = Movie.new('Inception', 'Thriller')
puts movie.chain

movie = Movie.new('Terminal', 'Comedy')
puts movie.chain

movie = Movie.new('Lord of the Ring', 'Tale')
puts movie.chain
```

## Tests
```ruby
describe Movie do
  let(:horror) { Movie.new('It', 'Horror')}
  let(:thriller) { Movie.new('Inception', 'Thriller')}
  let(:comedy) { Movie.new('Terminal', 'Comedy')}
  let(:tale) { Movie.new('Lord of the Ring', 'Tale')}
  let(:test_error) { Movie.new('Test Error', 'Error Genre')}

  specify 'should return age limit for every genre' do
    expect(horror.chain).to eq('Horror It has age limit 18+')
    expect(thriller.chain).to eq('Thriller Inception has age limit 16+')
    expect(comedy.chain).to eq('Comedy Terminal has age limit 6+')
    expect(tale.chain).to eq('Tale Lord of the Ring has age limit 0+')
  end

  specify 'raises an error if unknown genre' do
    error_msg = 'Only for Horror, Thriller, Comedy and Tale'
    expect { test_error.chain }.to raise_error error_msg
  end
end
```
