# Iterator Pattern
The Iterator pattern is pattern in which an iterator is used to traverse a container and access the container's elements.

## Problem
We need to create a Movies Player with possibility to consistently pass through movies.

## Solution
The **Iterator Pattern** lets us access the elements of an aggregate object sequentially without exposing its underlying representation 

## Example
Example Movies Player shows implementation the Iterator pattern.

```ruby
# MyMovie
class MyMovie
  attr_accessor :title, :genre, :year
  def initialize(title, genre, year)
    self.title = title
    self.genre = genre
    self.genre = year
  end

  def movie_info
    "Movie: #{title}, genre: #{genre}, year #{year}"
  end
end

# Movies Collection
class MoviesCollection
  attr_accessor :movies
  def initialize
    self.movies = []
  end

  def add_movie(movie)
    movies.push(movie)
  end

  def all_movies
    movies
  end
end

# Movies Player (implement Flyweight pattern)
class MoviesPlayer
  attr_accessor :movies_collection, :movie_id
  def initialize(movies_collection)
    self.movies_collection = movies_collection.all_movies
    self.movie_id = 0
  end

  def current_movie
    return movies_collection[movie_id] if movies_collection[movie_id]
    raise('Error! This movie does not exist!')
  end

  def next_movie
    if movies_collection[movie_id + 1]
      self.movie_id += 1
      return movies_collection[movie_id]
    end
    raise('Error! Next movie does not exist!')
  end

  def previous_movie
    if movies_collection[movie_id - 1]
      self.movie_id -= 1
      return movies_collection[movie_id]
    end
    raise('Error! Previous movie does not exist!')
  end
end

movies_collection = MoviesCollection.new

movies_collection.add_movie(
  MyMovie.new('Avengers: Infinity War', 'Fantasy/Science fiction', 2018)
)
movies_collection.add_movie(
  MyMovie.new('Black Panther', 'Fantasy/Science fiction', 2018)
)
movies_collection.add_movie(
  MyMovie.new('Tomb Raider', 'Fantasy/Action', 2018)
)

movies_player = MoviesPlayer.new(movies_collection)
puts movies_player.current_movie.movie_info
puts movies_player.next_movie.movie_info
puts movies_player.next_movie.movie_info
puts movies_player.previous_movie.movie_info
puts movies_player.previous_movie.movie_info
puts movies_player.next_movie.movie_info

```

## Tests
```ruby
describe MoviesPlayer do
  let(:movies_collection) do
    MoviesCollection.new
  end
  let(:movie_avengers) do
    MyMovie.new(
      'Avengers: Infinity War',
      'Fantasy/Science fiction',
      2018
    )
  end
  let(:movie_black_panther) do
    MyMovie.new(
      'Black Panther',
      'Fantasy/Science fiction',
      2018
    )
  end
  let(:movie_tomb_raider) do
    MyMovie.new('Tomb Raider', 'Fantasy/Action', 2018)
  end
  let(:text_avengers) do
    'Movie: Avengers: Infinity War, genre: Fantasy/Science fiction, year 2018'
  end
  let(:text_black_panther) do
    'Movie: Black Panther, genre: Fantasy/Science fiction, year 2018'
  end
  let(:text_tomb_raider) do
    'Movie: Tomb Raider, genre: Fantasy/Action, year 2018'
  end
  let(:err_next) do
    'Error! Next movie does not exist!'
  end
  let(:err_prev) do
    'Error! Previous movie does not exist!'
  end

  specify 'should return movie info' do
    movies_collection.add_movie(movie_avengers)
    movies_collection.add_movie(movie_black_panther)
    movies_collection.add_movie(movie_tomb_raider)
    movies_player = MoviesPlayer.new(movies_collection)
    expect(movies_player.current_movie.movie_info).to eq(text_avengers)
    expect(movies_player.next_movie.movie_info).to eq(text_black_panther)
    expect(movies_player.next_movie.movie_info).to eq(text_tomb_raider)
    expect { movies_player.next_movie.movie_info }.to raise_error err_next
    expect(movies_player.previous_movie.movie_info).to eq(text_black_panther)
    expect(movies_player.previous_movie.movie_info).to eq(text_avengers)
    expect { movies_player.previous_movie.movie_info }.to raise_error err_prev
  end
end
```
