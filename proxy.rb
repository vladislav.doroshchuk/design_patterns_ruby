# Movie
class Movie
  attr_accessor :title, :age_limit

  def initialize(title, age_limit)
    self.title = title
    self.age_limit = age_limit
  end
end

# Client
class Client
  attr_accessor :name, :age

  def initialize(name, age)
    self.name = name
    self.age = age
  end
end

# Theater
class Theater
  attr_accessor :movie, :client

  def initialize(movie, client)
    self.movie = movie
    self.client = client
  end

  def sell_a_ticket
    "Client #{client.name} bought a ticket on movie #{movie.title}"
  end
end

# TheaterProxy
class TheaterProxy
  attr_accessor :movie, :client

  def initialize(movie, client)
    self.movie = movie
    self.client = client
  end

  def sell_a_ticket
    if movie.age_limit <= client.age
      return "Client #{client.name} bought a ticket on movie #{movie.title}"
    end
     "Buying a ticket is forbidden (restriction on age)"
  end
end

movie = Movie.new("It", 18)
client = Client.new("Bill", 15)

theater = Theater.new(movie, client)
puts theater.sell_a_ticket

theater = TheaterProxy.new(movie, client)
puts theater.sell_a_ticket
