# Sort Module
module Sort
  attr_accessor :sort_array
  def initialize(sort_array)
    @sort_array = sort_array
  end

  def swap!(inx_l, inx_r)
    sort_array[inx_l], sort_array[inx_r] = sort_array[inx_r], sort_array[inx_l]
  end

  def array_sort
    result = array_sorting.join(',')
    "Class #{self.class} result :#{result}"
  end
end

# Sort Bubble
class Bubble
  include Sort

  def array_sorting(swapped = true)
    while swapped
      swapped = false
      sort_array.each_with_index do |right_element, index|
        next unless index > 0
        swapped = bubble_sort(right_element, index, swapped)
      end
    end
    sort_array
  end

  private

  def bubble_sort(right_element, index, swapped)
    left_element = sort_array[index - 1]
    if left_element > right_element
      swap!(index - 1, index)
      swapped = true
    end
    swapped
  end
end

# Sort Counting
class Counting
  include Sort
  def array_sorting
    array_counting = []
    sort_array.each do |value,|
      array_counting = filling_array_counting(value, array_counting)
    end
    sort_from_array_counting array_counting
  end

  private

  def filling_array_counting(value, array_counting)
    array_counting[value] = 0 if array_counting[value].nil?
    array_counting[value] += 1
    array_counting
  end

  def sort_from_array_counting(array_counting, result = [])
    array_counting.each_with_index do |value, index|
      next if value.nil?
      result << [index] * value
    end
    result
  end
end

# Sort Insertion
class Insertion
  include Sort
  def array_sorting
    sort_array.each_with_index do |_, index|
      insertion_sort(index)
    end
    sort_array
  end

  private

  def insertion_sort(sorted_index)
    index = sorted_index
    while index > 0 && sort_array[index] < sort_array[index - 1]
      swap!(index, index - 1)
      index -= 1
    end
  end
end

# Sort Strategy
class SortStrategy
  attr_accessor :strategy

  def initialize(strategy)
    @strategy = strategy
  end

  def sort
    strategy.array_sort
  end
end

array_example = [3, 44, 38, 5, 47, 15, 36, 26]

strategy_insertion = Insertion.new(array_example)
sort_strategy = SortStrategy.new(strategy_insertion)
puts sort_strategy.sort

strategy_bubble = Bubble.new(array_example)
sort_strategy.strategy = strategy_bubble
puts sort_strategy.sort

strategy_counting = Counting.new(array_example)
sort_strategy.strategy = strategy_counting
puts sort_strategy.sort
