# Movie Information
class MovieInformation
  attr_accessor :title, :genre, :age_limit

  def initialize(title, genre)
    self.title = title
    self.genre = genre
  end

  def message
    "#{genre} #{title} has age limit #{age_limit}+"
  end
end

# Horror Movie
class HorrorMovie
  attr_accessor :next_chain
  def movie_age_limit(movie)
    if movie.genre == 'Horror'
      movie.age_limit = 18
    else
      next_chain.movie_age_limit(movie)
    end
  end

  def next_in_chain(next_chain)
    self.next_chain = next_chain
  end
end

# Thriller Movie
class ThrillerMovie
  attr_accessor :next_chain
  def movie_age_limit(movie)
    if movie.genre == 'Thriller'
      movie.age_limit = 16
    else
      next_chain.movie_age_limit(movie)
    end
  end

  def next_in_chain(next_chain)
    self.next_chain = next_chain
  end
end

# Comedy Movie
class ComedyMovie
  attr_accessor :next_chain
  def movie_age_limit(movie)
    if movie.genre == 'Comedy'
      movie.age_limit = 6
    else
      next_chain.movie_age_limit(movie)
    end
  end

  def next_in_chain(next_chain)
    self.next_chain = next_chain
  end
end

# Tale Movie
class TaleMovie
  attr_accessor :next_chain
  def movie_age_limit(movie)
    raise('Only for Horror, Thriller, Comedy and Tale') if movie.genre != 'Tale'
    movie.age_limit = 0
  end
end

# Movie
class Movie
  attr_accessor :title, :genre

  def initialize(title, genre)
    self.title = title
    self.genre = genre
  end

  def chain
    horror = HorrorMovie.new
    thriller = ThrillerMovie.new
    comedy = ComedyMovie.new
    tale = TaleMovie.new
    horror.next_in_chain(thriller)
    thriller.next_in_chain(comedy)
    comedy.next_in_chain(tale)
    movie_information = MovieInformation.new(title, genre)
    horror.movie_age_limit(movie_information)
    movie_information.message
  end
end
