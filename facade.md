# Facade Pattern
The Facade pattern lets provide a simplified interface to a complex system of classes, library or framework.

## Problem
We need to create simple interface to a different classes for distribute of information.

## Solution
The **Facade** pattern provide us a simplified interface in our Deliver System to a complex system of classes.

## Example
DeliverFacade implements Facade Pattern

```ruby
# post message on Facebook
class FacebookMessage
  def post(user, title)
    puts "User #{user} post in Facebook the message \"#{title}\""
  end
end

# email
class Email
  def email(user, title, email)
    puts "User #{user} send to address #{email} email \"#{title}\""
  end
end

# write in Telegram
class Telegram
  def write(user, message, chat_id)
    puts "Sser #{user} write down \"#{message}\" in Telegram in chat #{chat_id}"
  end
end

# message deliver by crow
class CrowsDeliveryCompany
  def send_crow(message, lord_name, castle)
    puts "Send letter \"#{message}\" from lord #{lord_name} to #{castle}"
  end
end

# The Deliver Facade
class DeliverFacade
  attr_accessor :facebook, :email, :telegram, :crow

  def initialize(facebook, email, telegram, crow)
    self.facebook = facebook
    self.email = email
    self.telegram = telegram
    self.crow = crow
  end

  def deliver_part_one(user, title, message, email_address, chat_id)
    facebook.post(user, title)
    email.email(user, title, email_address)
    telegram.write(user, message, chat_id)
  end

  def deliver_part_two(user, title, message, lord_name, castle)
    facebook.post(user, title)
    crow.send_crow(message, lord_name, castle)
  end
end

facebook = FacebookMessage.new
email = Email.new
telegram = Telegram.new
crow = CrowsDeliveryCompany.new

deliver_facade = DeliverFacade.new(facebook, email, telegram, crow)
deliver_facade.deliver_part_one(
  'Sam',
  'The Winds of Winter',
  'Winter is coming',
  'test@test.com',
  123
)
deliver_facade.deliver_part_two(
  'Sam',
  'The wall was destroyed',
  'The White Walkers came to a Westeros',
  'John Snow',
  'all lords'
)
```

## Tests
```ruby
describe DeliverFacade do
  let(:facebook) { FacebookMessage.new }
  let(:email) { Email.new }
  let(:telegram) { Telegram.new }
  let(:crow) { CrowsDeliveryCompany.new }

  specify 'should deliver first part of messages' do
    deliver_facade = DeliverFacade.new(facebook, email, telegram, crow)
    deliver_part_one = deliver_facade.deliver_part_one(
      'Sam',
      'The Winds of Winter',
      'Winter is coming',
      'test@te.com',
      123
    )
    expect(deliver_part_one.size).to eq(3)
    message = 'User Sam post in Facebook the message "The Winds of Winter"'
    expect(deliver_part_one[0]).to eq(message)
    message = 'User Sam send to address test@te.com email "The Winds of Winter"'
    expect(deliver_part_one[1]).to eq(message)
    message = 'User Sam write down "Winter is coming" in Telegram in chat 123'
    expect(deliver_part_one[2]).to eq(message)
  end

  specify 'should deliver second part of messages' do
    deliver_facade = DeliverFacade.new(facebook, email, telegram, crow)
    deliver_part_two = deliver_facade.deliver_part_two(
      'Sam',
      'The wall was destroyed',
      'The White Walkers came',
      'John Snow',
      'all'
    )
    expect(deliver_part_two.size).to eq(2)
    message = 'User Sam post in Facebook the message "The wall was destroyed"'
    expect(deliver_part_two[0]).to eq(message)
    message = 'Send letter "The White Walkers came" from lord John Snow to all'
    expect(deliver_part_two[1]).to eq(message)
  end
end
```
