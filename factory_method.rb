# Button
class Button
  attr_accessor :color, :type
  def initialize(color = 'green')
    @color = color
  end
end

# Windows Button
class WindowsButton < Button
  def render_button
    self.type = "Windows"
    puts "Get Windows Button color #{color}"
  end

  def on_click
    puts 'Click Windows Button'
  end
end

# HTML Button
class HTMLButton < Button
  def render_button
    self.type = "HTML"
    puts "Get HTML Button color #{color}"
  end

  def on_click
    puts 'Click HTML Button'
  end
end

# Dialog
class Dialog
  def render_window(button_color)
    button_ok = create_button(button_color)
    button_ok.render_button
    button_ok.on_click
    button_ok
  end
end

# Windows Dialog
class WindowsDialog < Dialog
  def create_button(button_color)
    WindowsButton.new(button_color)
  end
end

# HTML Dialog
class HTMLDialog < Dialog
  def create_button(button_color)
    HTMLButton.new(button_color)
  end
end

# Client Application
class ClientApplication
  attr_accessor :dialog, :config
  def initialize(config)
    case config
    when 'windows'
      @dialog = WindowsDialog.new
      when 'html'
      @dialog = HTMLDialog.new
    else
      raise('Error! Unknown operating system!')
    end
  end

  def main(button_color)
    dialog.render_window(button_color)
  end
end

app = ClientApplication.new('windows')
app.main('red')

