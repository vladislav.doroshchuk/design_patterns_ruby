# Button
class Button
  attr_accessor :type_element
end

# WinButton
class WinButton < Button
  def render
    self.type_element = 'WinButton'
  end
end

# MacButton
class MacButton < Button
  def render
    self.type_element = 'MacButton'
  end
end

# Checkbox
class Checkbox
  attr_accessor :type_element
end

# WinCheckbox
class WinCheckbox < Checkbox
  def render
    self.type_element = 'WinCheckbox'
  end
end

# Mac Checkbox
class MacCheckbox < Checkbox
  def render
    self.type_element = 'MacCheckbox'
  end
end

# GUIFactory
class GUIFactory
end

# Win Factory
class WinFactory < GUIFactory
  def create_button
    WinButton.new
  end

  def create_checkbox
    WinCheckbox.new
  end
end

# Mac Factory
class MacFactory < GUIFactory
  def create_button
    MacButton.new
  end

  def create_checkbox
    MacCheckbox.new
  end
end

# Application
class Application
  attr_accessor :button, :factory, :checkbox

  def initialize(factory)
    @factory = factory
  end

  def create_ui
    self.button = factory.create_button
    self.checkbox = factory.create_checkbox
  end

  def render
    "button: #{button.render}, checkbox: #{checkbox.render}"
  end
end

# Application Configurator
class ApplicationConfigurator
  attr_accessor :app, :factory

  def initialize(operating_system)
    case operating_system
    when 'windows'
      @factory = WinFactory.new
    when 'mac'
      @factory = MacFactory.new
    else
      raise('Error! Unknown operating system!')
    end
  end

  def main
    Application.new(factory)
  end
end

%w[windows mac].each do |operating_system|
  app_config = ApplicationConfigurator.new(operating_system)
  app = app_config.main
  app.create_ui
  puts app.render
end
