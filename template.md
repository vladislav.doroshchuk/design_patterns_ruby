# Template Pattern
The Template pattern is a pattern that defines the program skeleton of an algorithm in an operation, deferring some steps to subclasses. 
It lets one redefine certain steps of an algorithm without changing the algorithm's structure.

## Problem
We have to change one step of algorithm of creation dialog.

## Solution
The **Template Pattern** lets us redefine certain steps of an algorithm without changing the algorithm's structure.

## Example
This example implement the Template pattern and use in our dialog HTML Button or Windows Button when it need

```ruby
# Dialog Template
class DialogTemplate
  def generate_dialog!
    "#{title_dialog} #{render_button} #{information}"
  end

  private

  def title_dialog
    'Dialog'
  end

  def render_button
    raise('Error! Not implemented.')
  end

  def information
    'Information'
  end
end

# HTML Button
class HTMLButtonDialog < DialogTemplate
  def render_button
    'HTML Button'
  end
end

# Windows Button
class WindowsButtonDialog < DialogTemplate
  def render_button
    'Windows Button'
  end
end

windows_dialod = HTMLButtonDialog.new
puts windows_dialod.generate_dialog!

html_dialod = WindowsButtonDialog.new
puts html_dialod.generate_dialog!
```

## Tests
```ruby
describe DialogTemplate do
  let(:windows_dialog) { WindowsButtonDialog.new }
  let(:html_dialog) { HTMLButtonDialog.new }
  let(:template_dialog) { DialogTemplate.new }
  let(:dialog_widows_msg) { 'Dialog Windows Button Information' }
  let(:dialog_html_msg) { 'Dialog HTML Button Information' }

  specify 'should return dialogs with needed button' do
    expect(windows_dialog.generate_dialog!).to eq(dialog_widows_msg)
    expect(html_dialog.generate_dialog!).to eq(dialog_html_msg)
  end

  specify 'raises an error if use template dialog' do
    error_msg = 'Error! Not implemented.'
    expect { template_dialog.generate_dialog! }.to raise_error error_msg
  end
end
```
