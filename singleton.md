# Singleton Pattern
The Singleton pattern ensures that a class has only one instance and provides a global point of access to that instance

## Problem
We need to be sure that a class has only one instance with global point of access

## Solution
The **Singleton Pattern** help us create only one instance of class

## Example
We create only one Settings instance with global point of access 

```ruby
# Settings
class Settings
  @@instance = Settings.new

  def self.instance
    @@instance
  end

  private_class_method :new
end

settings_first = Settings.instance
puts settings_first
settings_second = Settings.instance
puts settings_second # output the same instance as settings_first

Settings.new # throws an error
```

## Tests
```ruby
describe Settings do
  let(:settings_first) { Settings.instance }

  specify 'should create only one Settings instance' do
    expect(settings_first).to be_an_instance_of(Settings)
    settings_second = Settings.instance
    expect(settings_first).to eq(settings_second)
  end

  specify 'raises an error if try to create new Settings instance' do
    error_msg = "private method `new' called for Settings:Class"
    expect { Settings.new }.to raise_error error_msg
  end
end
```
