# Decorator Pattern
The Decorator pattern allows behavior to be added to an individual object, either statically or dynamically, without affecting the behavior of other objects from the same class.

## Problem
We need to add the behavior to some of Author objects without affecting the behavior of other objects from the same class.

## Solution
The **Decorator Pattern** help us add individual object AuthorDecorator for cope the problem.

## Example
AuthorDecorator implement the Decorator pattern

```ruby
# Author
class Author
  def initialize(first_name, last_name, phone_code, phone_number)
    @first_name = first_name
    @last_name = last_name
    @phone_code = phone_code
    @phone_number = phone_number
  end

  attr_accessor :first_name, :last_name, :phone_code, :phone_number
end

# AuthorDecorator
class AuthorDecorator < SimpleDelegator
  def full_name
    "#{first_name} #{last_name}"
  end

  def full_phone
    "+(#{phone_code})#{phone_number}"
  end
end

author = Author.new('Name', 'Surname', 100, '123-45-67')
author_decorator = AuthorDecorator.new(author)
puts author_decorator.full_name
puts author_decorator.full_phone
```

## Tests
```ruby
describe AuthorDecorator do
  let(:author) { Author.new('Name', 'Surname', 100, '123-45-67') }

  specify 'should expand the Author with full_name and full_phone' do
    author_decorator = AuthorDecorator.new(author)
    expect(author_decorator.full_name).to eq('Name Surname')
    expect(author_decorator.full_phone).to eq('+(100)123-45-67')
  end
end
```
