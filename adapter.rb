# Screwdriver
class Screwdriver
  def unscrew!
    'Screwdriver unscrewed the screw'
  end
end

# Tool
class Tool
  def initialize(adapter)
    @adapter = adapter
  end

  def use_tool
    @adapter.use_tool
  end
end

# Screwdriver Adapter
class ScrewdriverAdapter
  def initialize(screwdriver)
    @screwdriver = screwdriver
  end

  def use_tool
    @screwdriver.unscrew!
  end
end

screwdriver = Screwdriver.new
screwdriver_adapter = ScrewdriverAdapter.new(screwdriver)
tool = Tool.new(screwdriver_adapter)

puts tool.use_tool
