# Proxy Pattern
The Proxy pattern provide a substitute or placeholder for another object to control access to it.

## Problem
We need to create object with same interface as a Theater but with possibility to checking client age 

## Solution
The **Proxy Pattern** help us create TheaterProxy which can substitute a Theater

## Example
We create TheaterProxy with same interface as a Theater which check client age (it sell ticket only if clients age is over then age limit) 

```ruby
# Movie
class Movie
  attr_accessor :title, :age_limit

  def initialize(title, age_limit)
    self.title = title
    self.age_limit = age_limit
  end
end

# Client
class Client
  attr_accessor :name, :age

  def initialize(name, age)
    self.name = name
    self.age = age
  end
end

# Theater
class Theater
  attr_accessor :movie, :client

  def initialize(movie, client)
    self.movie = movie
    self.client = client
  end

  def sell_a_ticket
    "Client #{client.name} bought a ticket on movie #{movie.title}"
  end
end

# TheaterProxy
class TheaterProxy
  attr_accessor :movie, :client

  def initialize(movie, client)
    self.movie = movie
    self.client = client
  end

  def sell_a_ticket
    if movie.age_limit <= client.age
      return "Client #{client.name} bought a ticket on movie #{movie.title}"
    end
     "Buying a ticket is forbidden (restriction on age)"
  end
end

movie = Movie.new("It", 18)
client = Client.new("Bill", 15)

theater = Theater.new(movie, client)
puts theater.sell_a_ticket

theater = TheaterProxy.new(movie, client)
puts theater.sell_a_ticket
```

## Tests
```ruby
describe TheaterProxy do
  let(:movie) { Movie.new("It", 18)}
  let(:client_bill) { Client.new("Bill", 15)}
  let(:client_sam) { Client.new("Sam", 20)}

  specify 'should sell tickets Bill and Sam' do
    theater = Theater.new(movie, client_bill)
    expect(theater.sell_a_ticket).to eq('Client Bill bought a ticket on movie It')
    theater = Theater.new(movie, client_sam)
    expect(theater.sell_a_ticket).to eq('Client Sam bought a ticket on movie It')
  end

  specify 'should sell ticket only to Sam' do
    theater = TheaterProxy.new(movie, client_bill)
    expect(theater.sell_a_ticket).to eq('Buying a ticket is forbidden (restriction on age)')
    theater = TheaterProxy.new(movie, client_sam)
    expect(theater.sell_a_ticket).to eq('Client Sam bought a ticket on movie It')
  end
end
```
