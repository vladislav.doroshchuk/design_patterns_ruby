# Visitor Pattern
The Visitor pattern is a way of separating an algorithm from an object structure on which it operates. 
A practical result of this separation is the ability to add new operations to existent object structures without modifying the structures.

## Problem
We have to create algorithm Journey with different sights and ability to add new operations to existent object.

## Solution
The **Visitor Pattern** lets us separating an algorithm from an object structure on which it operates.

## Example
This example implement the Visitor pattern and we can use two "visitors" SightsVisitor and SightsPhotoVisitor

```ruby
# Sights
class Sights
  attr_reader :name
  def initialize(name)
    @name = name
  end

  def visit(visitor)
    visitor.visit(self)
  end
end

# Temple
class Temple < Sights
  def doing
    'prays'
  end
end

# Stadium
class Stadium < Sights
  def doing
    'cheer'
  end
end

# Theater
class Theater < Sights
  def doing
    'applaud'
  end
end

# Journey
class Journey
  def initialize
    @sights = []
  end

  def add_sight(sight)
    @sights << sight
  end

  def visit(visitor)
    @sights.each do |element|
      element.visit(visitor)
    end
  end
end

# SightsVisitor
class SightsVisitor
  def visit(sights)
    puts "Visiting: #{sights.name} and #{sights.doing} there"
  end
end

# SightsPhotoVisitor
class SightsPhotoVisitor
  def visit(sights)
    puts "Visiting with camera: #{sights.name} and #{sights.doing} there"
  end
end

temple = Temple.new('Temple')
stadium = Stadium.new('Stadium')
theater = Theater.new('Theater')

journey = Journey.new
journey.add_sight(temple)
journey.add_sight(stadium)
journey.add_sight(theater)

sights_visitor = SightsVisitor.new
journey.visit(sights_visitor)

sights_visitor = SightsPhotoVisitor.new
journey.visit(sights_visitor)
```

## Tests
```ruby
describe Journey do
  let(:temple) { Temple.new('Temple') }
  let(:stadium) { Stadium.new('Stadium') }
  let(:theater) { Theater.new('Theater') }
  let(:journey) { Journey.new }
  let(:sights_visitor) { SightsVisitor.new }
  let(:sights_photo_visitor) { SightsPhotoVisitor.new }
  let(:sights_visitor_msg) do
    [
      'Visiting: Temple and prays there',
      'Visiting: Stadium and cheer there',
      'Visiting: Theater and applaud there'
    ]
  end
  let(:sights_photo_visitor_msg) do
    [
      'Visiting with camera: Temple and prays there',
      'Visiting with camera: Stadium and cheer there',
      'Visiting with camera: Theater and applaud there'
    ]
  end

  specify 'should visit all sights' do
    add_sights
    check_msg sights_visitor, sights_visitor_msg
  end

  specify 'should visit all sights with a camera' do
    add_sights
    check_msg sights_photo_visitor, sights_photo_visitor_msg
  end

  def check_msg(sights, sights_msg)
    sights_msg.each do |msg|
      STDOUT.should_receive(:puts).with(msg)
    end
    journey.visit(sights)
  end

  def add_sights
    journey.add_sight(temple)
    journey.add_sight(stadium)
    journey.add_sight(theater)
  end
end
```
