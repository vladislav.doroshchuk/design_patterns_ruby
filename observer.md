# Observer Pattern
The Observer pattern is pattern in which an object, called the subject, maintains a list of its dependents, called observers, 
and notifies them automatically of any state changes, usually by calling one of their methods.

## Problem
We have to create a informer the spectators about new movies

## Solution
The **Observer Pattern** lets us create classes Cinema and Spectator and inform spectators about new movie in cinema

## Example
in example is implemented the Observer pattern.

```ruby
class Spectator
  attr_accessor :name

  def initialize(name)
    @name = name
  end

  def inform(cinema)
    "Spectator #{name} is informed about new movie: #{cinema.movie}"
  end
end

class Cinema
  attr_accessor :spectators, :movie
  def initialize
    @spectators = []
    @movie = ''
  end

  def add_spectator(spectator)
    spectators << spectator
  end

  def remove_spectator(spectator_id)
    spectators.delete_at spectator_id
  end

  def notify
    inform_log = []
    spectators.each do |spectator|
      inform_log << spectator.inform(self)
    end
    inform_log
  end
end

cinema = Cinema.new
cinema.add_spectator(Spectator.new("Jon"))
cinema.add_spectator(Spectator.new("Jessy"))
cinema.add_spectator(Spectator.new("Tom"))

cinema.movie = "The Crimes of Grindelwald"
cinema.remove_spectator(0)

cinema.notify.each_with_index do |value, index|
  puts "index: #{index}, value: #{value}"
end

```

## Tests
```ruby
describe Spectator do
  let(:cinema) { Cinema.new }

  specify 'add spectators and inform them' do
    spectators = %w[Jon Jessy Tom]
    spectators.each do |name|
      cinema.add_spectator(Spectator.new(name))
    end
    check_notify cinema, spectators, 3
    cinema.remove_spectator(0)
    check_notify cinema, %w[Jessy Tom], 2
  end

  def check_notify(cinema, spectators, number)
    cinema.movie = 'It'
    expect(cinema.notify.size).to eq(number)
    cinema.notify.each_with_index do |value, index|
      msg = "Spectator #{spectators[index]} is informed about new movie: It"
      expect(value).to eq(msg)
    end
  end
end
```
