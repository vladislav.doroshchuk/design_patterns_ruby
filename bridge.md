# Bridge Pattern
The Bridge pattern decouple an abstraction from its implementation so that the two can vary independently.

## Problem
We need to separate abstraction from implementation(we will create devices and remotes).

## Solution
The **Bridge Pattern** help us create devices(TV, Radio) and remotes for them so that the two can vary independently.

## Example
We create classes Remote (represent abstraction) and Device (implementation)

```ruby
# Remote
class Remote
  attr_accessor :device

  def initialize(device)
    @device = device
  end

  def toggle_power
    if device.device_is_enabled?
      device.device_disable
    else
      device.device_enable
    end
  end

  def volume_down
    device.volume -= 10
  end

  def volume_up
    device.volume += 10
  end

  def channel_down
    device.channel -= 1
  end

  def channel_up
    device.channel += 1
  end
end

# AdvancedRemote
class AdvancedRemote < Remote
  def mute
    device.volume = 0
    puts "#{device.device_type} sound off"
  end
end

# Device
class Device
  attr_accessor :enable, :device_type, :volume, :channel

  def initialize(enable = true, volume = 20, channel = 10)
    @enable = enable
    @volume = volume
    @channel = channel
  end

  def device_is_enabled?
    enable
  end

  def device_enable
    self.enable = true
    puts "#{device_type}  is enabled"
  end

  def device_disable
    self.enable = false
    puts "#{device_type} is disable"
  end
end

# Tvset
class Tvset < Device
  def initialize
    @device_type = 'Tvset'
  end
end

# Radio
class Radio < Device
  def initialize
    @device_type = 'Radio'
  end
end

tv = Tvset.new
remote = Remote.new(tv)
remote.toggle_power
remote.toggle_power
remote.toggle_power

radio = Radio.new
remote = AdvancedRemote.new(radio)
remote.mute
remote.toggle_power
remote.toggle_power
```

## Tests
```ruby
describe Remote do
  specify 'should create remote for TV-set and toggle_power it' do
    tv = Tvset.new
    remote = Remote.new(tv)
    remote.toggle_power
    expect(tv.enable).to be_falsey
    remote.toggle_power
    expect(tv.enable).to be_truthy
  end

  specify 'should create remote for Radio, toggle_power it' do
    radio = Radio.new
    remote = Remote.new(radio)
    remote.toggle_power
    expect(radio.enable).to be_truthy
    remote.toggle_power
    expect(radio.enable).to be_falsey
  end
end

describe AdvancedRemote do
  specify 'should create advanced remote for Radio and mute it' do
    enable = true
    volume = 20
    channel = 2
    radio = Radio.new(enable, volume, channel)
    remote = AdvancedRemote.new(radio)

    expect(radio.volume).to eq(volume)
    remote.volume_down
    expect(radio.volume).to eq(volume - 10)
    remote.volume_up
    expect(radio.volume).to eq(volume)
    remote.mute
    expect(radio.volume).to eq(0)

    expect(radio.channel).to eq(channel)
    remote.channel_down
    expect(radio.channel).to eq(channel - 1)
    remote.channel_up
    expect(radio.channel).to eq(channel)
  end
end
```
