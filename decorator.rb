# Author
class Author
  def initialize(first_name, last_name, phone_code, phone_number)
    @first_name = first_name
    @last_name = last_name
    @phone_code = phone_code
    @phone_number = phone_number
  end

  attr_accessor :first_name, :last_name, :phone_code, :phone_number
end

# AuthorDecorator
class AuthorDecorator < SimpleDelegator
  def full_name
    "#{first_name} #{last_name}"
  end

  def full_phone
    "+(#{phone_code})#{phone_number}"
  end
end

author = Author.new('Name', 'Surname', 100, '123-45-67')
author_decorator = AuthorDecorator.new(author)
puts author_decorator.full_name
puts author_decorator.full_phone
