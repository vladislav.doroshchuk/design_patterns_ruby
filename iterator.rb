# MyMovie
class MyMovie
  attr_accessor :title, :genre, :year
  def initialize(title, genre, year)
    self.title = title
    self.genre = genre
    self.year = year
  end

  def movie_info
    "Movie: #{title}, genre: #{genre}, year #{year}"
  end
end

# Movies Collection
class MoviesCollection
  attr_accessor :movies
  def initialize
    self.movies = []
  end

  def add_movie(movie)
    movies.push(movie)
  end

  def all_movies
    movies
  end
end

# Movies Player (implement Flyweight pattern)
class MoviesPlayer
  attr_accessor :movies_collection, :movie_id
  def initialize(movies_collection)
    self.movies_collection = movies_collection.all_movies
    self.movie_id = 0
  end

  def current_movie
    return movies_collection[movie_id] if movies_collection[movie_id]
    raise('Error! This movie does not exist!')
  end

  def next_movie
    if movies_collection[movie_id + 1]
      self.movie_id += 1
      return movies_collection[movie_id]
    end
    raise('Error! Next movie does not exist!')
  end

  def previous_movie
    if movies_collection[movie_id - 1] && movie_id > 0
      self.movie_id -= 1
      return movies_collection[movie_id]
    end
    raise('Error! Previous movie does not exist!')
  end
end

movies_collection = MoviesCollection.new

movies_collection.add_movie(
  MyMovie.new('Avengers: Infinity War', 'Fantasy/Science fiction', 2018)
)
movies_collection.add_movie(
  MyMovie.new('Black Panther', 'Fantasy/Science fiction', 2018)
)
movies_collection.add_movie(
  MyMovie.new('Tomb Raider', 'Fantasy/Action', 2018)
)

movies_player = MoviesPlayer.new(movies_collection)
puts movies_player.current_movie.movie_info
puts movies_player.next_movie.movie_info
puts movies_player.next_movie.movie_info
puts movies_player.previous_movie.movie_info
puts movies_player.previous_movie.movie_info
