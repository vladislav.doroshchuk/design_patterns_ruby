# post message on Facebook
class FacebookMessage
  def post(user, title)
    "User #{user} post in Facebook the message \"#{title}\""
  end
end

# email
class Email
  def email(user, title, email)
    "User #{user} send to address #{email} email \"#{title}\""
  end
end

# write in Telegram
class Telegram
  def write(user, message, chat_id)
    "User #{user} write down \"#{message}\" in Telegram in chat #{chat_id}"
  end
end

# message deliver by crow
class CrowsDeliveryCompany
  def send_crow(message, lord_name, castle)
    "Send letter \"#{message}\" from lord #{lord_name} to #{castle}"
  end
end

# The Deliver Facade
class DeliverFacade
  attr_accessor :facebook, :email, :telegram, :crow, :delivery_pack

  def initialize(facebook, email, telegram, crow)
    self.facebook = facebook
    self.email = email
    self.telegram = telegram
    self.crow = crow
  end

  def deliver_part_one(user, title, message, email_address, chat_id)
    delivery_pack = []
    delivery_pack.push(facebook.post(user, title))
    delivery_pack.push(email.email(user, title, email_address))
    delivery_pack.push(telegram.write(user, message, chat_id))
  end

  def deliver_part_two(user, title, message, lord_name, castle)
    delivery_pack = []
    delivery_pack.push(facebook.post(user, title))
    delivery_pack.push(crow.send_crow(message, lord_name, castle))
  end
end

facebook = FacebookMessage.new
email = Email.new
telegram = Telegram.new
crow = CrowsDeliveryCompany.new

deliver_facade = DeliverFacade.new(facebook, email, telegram, crow)
puts deliver_facade.deliver_part_one(
  'Sam',
  'The Winds of Winter',
  'Winter is coming',
  'test@test.com',
  123
)
puts deliver_facade.deliver_part_two(
  'Sam',
  'The wall was destroyed',
  'The White Walkers came to a Westeros',
  'John Snow',
  'all lords'
)
