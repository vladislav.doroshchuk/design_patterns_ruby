# Pizza
class Pizza
  attr_accessor :dough, :sauce, :topping
end

# Pizza Builder
class PizzaBuilder
  attr_accessor :pizza
  def create_new_pizza_product
    @pizza = Pizza.new
  end
end

# Concrete Pizza Builder RoastedPorkPizzaBuilder
class RoastedPorkPizzaBuilder < PizzaBuilder
  def build_dough
    pizza.dough = 'cross'
  end

  def build_sauce
    pizza.sauce = 'tomato'
  end

  def build_topping
    pizza.topping = 'roasted pork'
  end
end

# Concrete Pizza Builder PepperoniPizzaBuilder
class PepperoniPizzaBuilder < PizzaBuilder
  def build_dough
    pizza.dough = 'pan baked'
  end

  def build_sauce
    pizza.sauce = 'chili'
  end

  def build_topping
    pizza.topping = 'pepperoni+salami'
  end
end

# Waiter
class Waiter
  attr_accessor :pizza_builder

  def pizza
    pizza_builder.pizza
  end

  def construct_pizza
    pizza_builder.create_new_pizza_product
    pizza_builder.build_dough
    pizza_builder.build_sauce
    pizza_builder.build_topping
  end
end

# A customer ordering a pizza.
class PizzaOrdering
  def order
    pizzas = []
    waiter = Waiter.new
    roasted_pork_pizza_builder = RoastedPorkPizzaBuilder.new
    pepperoni_pizza_builder = PepperoniPizzaBuilder.new
    waiter.pizza_builder = roasted_pork_pizza_builder
    waiter.construct_pizza
    pizzas << waiter.pizza
    waiter.pizza_builder = pepperoni_pizza_builder
    waiter.construct_pizza
    pizzas << waiter.pizza
  end
end

pizza_order = PizzaOrdering.new
pizza_order.order
