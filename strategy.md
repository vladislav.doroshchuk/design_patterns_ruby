# Strategy Pattern
The Strategy pattern (also known as the policy pattern) is a pattern that enables selecting an algorithm at runtime. 
Instead of implementing a single algorithm directly, code receives run-time instructions as to which in a family of algorithms to use.

## Problem
We have to create a classes which can use several sorting algorithms.

## Solution
The **Strategy Pattern** lets us take particular sorting algorithm at runtime.

## Example
This example implement the Strategy pattern and use Bubble, Counting or Insertion algorithm when it need

```ruby
# Sort Module
module Sort
  attr_accessor :sort_array
  def initialize(sort_array)
    @sort_array = sort_array
  end

  def swap!(inx_l, inx_r)
    sort_array[inx_l], sort_array[inx_r] = sort_array[inx_r], sort_array[inx_l]
  end

  def array_sort
    result = array_sorting.join(',')
    "Class #{self.class} result :#{result}"
  end
end

# Sort Bubble
class Bubble
  include Sort

  def array_sorting(swapped = true)
    while swapped
      swapped = false
      sort_array.each_with_index do |right_element, index|
        next unless index > 0
        swapped = bubble_sort(right_element, index, swapped)
      end
    end
    sort_array
  end

  private

  def bubble_sort(right_element, index, swapped)
    left_element = sort_array[index - 1]
    if left_element > right_element
      swap!(index - 1, index)
      swapped = true
    end
    swapped
  end
end

# Sort Counting
class Counting
  include Sort
  def array_sorting
    array_counting = []
    sort_array.each do |value,|
      array_counting = filling_array_counting(value, array_counting)
    end
    sort_from_array_counting array_counting
  end

  private

  def filling_array_counting(value, array_counting)
    array_counting[value] = 0 if array_counting[value].nil?
    array_counting[value] += 1
    array_counting
  end

  def sort_from_array_counting(array_counting, result = [])
    array_counting.each_with_index do |value, index|
      next if value.nil?
      result << [index] * value
    end
    result
  end
end

# Sort Insertion
class Insertion
  include Sort
  def array_sorting
    sort_array.each_with_index do |_, index|
      insertion_sort(index)
    end
    sort_array
  end

  private

  def insertion_sort(sorted_index)
    index = sorted_index
    while index > 0 && sort_array[index] < sort_array[index - 1]
      swap!(index, index - 1)
      index -= 1
    end
  end
end

# Sort Strategy
class SortStrategy
  attr_accessor :strategy

  def initialize(strategy)
    @strategy = strategy
  end

  def sort
    strategy.array_sort
  end
end

array_example = [3, 44, 38, 5, 47, 15, 36, 26]

strategy_insertion = Insertion.new(array_example)
sort_strategy = SortStrategy.new(strategy_insertion)
puts sort_strategy.sort

strategy_bubble = Bubble.new(array_example)
sort_strategy.strategy = strategy_bubble
puts sort_strategy.sort

strategy_counting = Counting.new(array_example)
sort_strategy.strategy = strategy_counting
puts sort_strategy.sort
```

## Tests
```ruby
describe SortStrategy do
  let(:array_example) { [3, 44, 38, 5, 47, 15, 36, 26] }
  let(:sort_result) { '3,5,15,26,36,38,44,47' }

  specify 'should sort using Insertion sorting algorithm' do
    strategy_insertion = Insertion.new(array_example)
    sort_strategy = SortStrategy.new(strategy_insertion)
    expect(sort_strategy.sort).to eq("Class Insertion result :#{sort_result}")
  end

  specify 'should sort using Bubble sorting algorithm' do
    strategy_bubble = Bubble.new(array_example)
    sort_strategy = SortStrategy.new(strategy_bubble)
    expect(sort_strategy.sort).to eq("Class Bubble result :#{sort_result}")
  end

  specify 'should sort using Counting sorting algorithm' do
    strategy_counting = Counting.new(array_example)
    sort_strategy = SortStrategy.new(strategy_counting)
    expect(sort_strategy.sort).to eq("Class Counting result :#{sort_result}")
  end
end
```
