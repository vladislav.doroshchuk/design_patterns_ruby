# Composite Pattern
The Composite pattern intent of a composite is to "compose" objects into tree structures to represent part-whole hierarchies.
Implementing the composite pattern lets clients treat individual objects and compositions uniformly.

## Problem
We need to create tree structures for TvSeries.

## Solution
The **Composite Pattern** help us add TvSeries, Seasons and Episodes to tree structures and navigate on tree.

## Example
TvSeriesComponent implement the Composite pattern

```ruby
class TvSeriesComponent
  attr_reader :children

  def initialize
    @children = []
  end

  def add_child(component)
    @children.push(component)
    component.parent = self
  end

  def get_child(index)
    @children[index]
  end
end

# TvSeries
class TvSeries < TvSeriesComponent
  attr_accessor :tv_series

  def initialize(tv_series)
    super()
    @tv_series = tv_series
  end
end

# Season
class Season < TvSeriesComponent
  attr_accessor :parent, :title

  def initialize(title)
    super()
    self.title = title
  end
end

# Episode
class Episode < TvSeriesComponent
  attr_accessor :parent, :title

  def initialize(title)
    super()
    self.title = title
  end
end

# black_mirror serial
black_mirror = TvSeries.new('Black Mirror')

# seasons of black_mirror serial
season_one = Season.new('S1')
season_two = Season.new('S2')
season_three = Season.new('S3')
season_four = Season.new('S4')

# add series to season
season_one.add_child Episode.new('The National Anthem')
season_one.add_child Episode.new('Fifteen Million Merits')
season_one.add_child Episode.new('The Entire History of You')
season_two.add_child Episode.new('Be Right Back')
season_two.add_child Episode.new('White Bear')
season_two.add_child Episode.new('The Waldo Moment')
season_two.add_child Episode.new('White Christmas')
season_three.add_child Episode.new('Nosedive')
season_three.add_child Episode.new('Getting Better')
season_three.add_child Episode.new('Playtest')
season_three.add_child Episode.new('Shut Up and Dance')
season_three.add_child Episode.new('San Junipero')
season_three.add_child Episode.new('Men Against Fire')
season_three.add_child Episode.new('Hated in the Nation')
season_four.add_child Episode.new('USS Callister')
season_four.add_child Episode.new('Arkangel')
season_four.add_child Episode.new('Crocodile')
season_four.add_child Episode.new('Hang the DJ')
season_four.add_child Episode.new('Metalhead')
season_four.add_child Episode.new('Black Museum')

# add seasons to serial
black_mirror.add_child season_one
black_mirror.add_child season_two
black_mirror.add_child season_three
black_mirror.add_child season_four

# navigate through tree
puts "TvSeries: #{black_mirror.tv_series}"
puts "Number of seasons: #{black_mirror.children.count}"
first_season = black_mirror.children.first
puts "First Season: #{first_season.title}"
puts "First Episode of First Season: #{first_season.children.first.title}"
black_mirror_s01_e01 = black_mirror.children.first.children.first
puts "Parent Season: #{black_mirror_s01_e01.parent.title}"
puts "Parent TvSeries: #{black_mirror_s01_e01.parent.parent.tv_series}"
```

## Tests
```ruby
describe TvSeriesComponent do
  let(:black_mirror) { TvSeries.new('Black Mirror') }
  let(:season_one) { Season.new('S1') }
  let(:season_two) { Season.new('S2') }

  specify 'should add series to season and seasons to serial and output them' do
    season_one.add_child Episode.new('The National Anthem')
    season_one.add_child Episode.new('Fifteen Million Merits')
    black_mirror.add_child season_one
    expect(black_mirror.tv_series).to eq('Black Mirror')
    expect(black_mirror.children[0].title).to eq('S1')
    expect(black_mirror.children[0].children[0].title).to eq('The National Anthem')
    expect(black_mirror.children[0].children[1].title).to eq('Fifteen Million Merits')
  end

  specify 'should return parent season and parent serial of series' do
    season_two.add_child Episode.new('Be Right Back')
    season_two.add_child Episode.new('White Bear')
    black_mirror.add_child season_two
    expect(black_mirror.children[0].title).to eq('S2')
    expect(black_mirror.children[0].children[0].title).to eq('Be Right Back')
    expect(black_mirror.children[0].children[1].title).to eq('White Bear')
    black_mirror_s02_e01 = black_mirror.children[0].children[1]
    expect(black_mirror_s02_e01.parent.title).to eq('S2')
    expect(black_mirror_s02_e01.parent.parent.tv_series).to eq('Black Mirror')
  end
end
```
