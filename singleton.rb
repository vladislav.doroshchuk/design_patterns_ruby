# Settings
class Settings
  @@instance = Settings.new

  def self.instance
    @@instance
  end

  private_class_method :new
end

settings_first = Settings.instance
puts settings_first
settings_second = Settings.instance
puts settings_second # output the same instance as settings_first

# Settings.new # throws an error
