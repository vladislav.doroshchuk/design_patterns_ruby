require 'rspec'
require '../bridge.rb'

describe Remote do
  specify 'should create remote for TV-set and toggle_power it' do
    tv = Tvset.new
    remote = Remote.new(tv)
    remote.toggle_power
    expect(tv.enable).to be_falsey
    remote.toggle_power
    expect(tv.enable).to be_truthy
  end

  specify 'should create remote for Radio, toggle_power it' do
    radio = Radio.new
    remote = Remote.new(radio)
    remote.toggle_power
    expect(radio.enable).to be_truthy
    remote.toggle_power
    expect(radio.enable).to be_falsey
  end
end

describe AdvancedRemote do
  specify 'should create advanced remote for Radio and mute it' do
    enable = true
    volume = 20
    channel = 2
    radio = Radio.new(enable, volume, channel)
    remote = AdvancedRemote.new(radio)

    expect(radio.volume).to eq(volume)
    remote.volume_down
    expect(radio.volume).to eq(volume - 10)
    remote.volume_up
    expect(radio.volume).to eq(volume)
    remote.mute
    expect(radio.volume).to eq(0)

    expect(radio.channel).to eq(channel)
    remote.channel_down
    expect(radio.channel).to eq(channel - 1)
    remote.channel_up
    expect(radio.channel).to eq(channel)
  end
end
