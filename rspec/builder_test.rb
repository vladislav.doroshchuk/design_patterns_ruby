require 'rspec'
require '../builder'

describe PizzaOrdering do
  let(:pizza_order) { PizzaOrdering.new }

  specify 'should return array with two Pizza instance' do
    pizza_order.order
    expect(pizza_order.order.size).to eq(2)
    pizza_order.order.each do |pizza|
      expect(pizza).to be_an_instance_of(Pizza)
    end
    expect(pizza_order.order[0].topping).to eq('roasted pork')
    expect(pizza_order.order[0].dough).to eq('cross')
    expect(pizza_order.order[0].sauce).to eq('tomato')

    expect(pizza_order.order[1].topping).to eq('pepperoni+salami')
    expect(pizza_order.order[1].dough).to eq('pan baked')
    expect(pizza_order.order[1].sauce).to eq('chili')
  end
end
