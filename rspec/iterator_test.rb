require 'rspec'
require '../iterator'

describe MoviesPlayer do
  let(:movies_collection) do
    MoviesCollection.new
  end
  let(:movie_avengers) do
    MyMovie.new(
      'Avengers: Infinity War',
      'Fantasy/Science fiction',
      2018
    )
  end
  let(:movie_black_panther) do
    MyMovie.new(
      'Black Panther',
      'Fantasy/Science fiction',
      2018
    )
  end
  let(:movie_tomb_raider) do
    MyMovie.new('Tomb Raider', 'Fantasy/Action', 2018)
  end
  let(:text_avengers) do
    'Movie: Avengers: Infinity War, genre: Fantasy/Science fiction, year 2018'
  end
  let(:text_black_panther) do
    'Movie: Black Panther, genre: Fantasy/Science fiction, year 2018'
  end
  let(:text_tomb_raider) do
    'Movie: Tomb Raider, genre: Fantasy/Action, year 2018'
  end
  let(:err_next) do
    'Error! Next movie does not exist!'
  end
  let(:err_prev) do
    'Error! Previous movie does not exist!'
  end

  specify 'should return movie info' do
    movies_collection.add_movie(movie_avengers)
    movies_collection.add_movie(movie_black_panther)
    movies_collection.add_movie(movie_tomb_raider)
    movies_player = MoviesPlayer.new(movies_collection)
    expect(movies_player.current_movie.movie_info).to eq(text_avengers)
    expect(movies_player.next_movie.movie_info).to eq(text_black_panther)
    expect(movies_player.next_movie.movie_info).to eq(text_tomb_raider)
    expect { movies_player.next_movie.movie_info }.to raise_error err_next
    expect(movies_player.previous_movie.movie_info).to eq(text_black_panther)
    expect(movies_player.previous_movie.movie_info).to eq(text_avengers)
    expect { movies_player.previous_movie.movie_info }.to raise_error err_prev
  end
end
