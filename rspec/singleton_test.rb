require 'rspec'
require '../singleton'

describe Settings do
  let(:settings_first) { Settings.instance }

  specify 'should create only one Settings instance' do
    expect(settings_first).to be_an_instance_of(Settings)
    settings_second = Settings.instance
    expect(settings_first).to eq(settings_second)
  end

  specify 'raises an error if try to create new Settings instance' do
    error_msg = "private method `new' called for Settings:Class"
    expect { Settings.new }.to raise_error error_msg
  end
end
