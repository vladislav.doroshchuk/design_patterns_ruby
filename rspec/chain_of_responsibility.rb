require 'rspec'
require '../chain_of_responsibility'

describe Movie do
  let(:horror) { Movie.new('It', 'Horror')}
  let(:thriller) { Movie.new('Inception', 'Thriller')}
  let(:comedy) { Movie.new('Terminal', 'Comedy')}
  let(:tale) { Movie.new('Lord of the Ring', 'Tale')}
  let(:test_error) { Movie.new('Test Error', 'Error Genre')}

  specify 'should return age limit for every genre' do
    expect(horror.chain).to eq('Horror It has age limit 18+')
    expect(thriller.chain).to eq('Thriller Inception has age limit 16+')
    expect(comedy.chain).to eq('Comedy Terminal has age limit 6+')
    expect(tale.chain).to eq('Tale Lord of the Ring has age limit 0+')
  end

  specify 'raises an error if unknown genre' do
    error_msg = 'Only for Horror, Thriller, Comedy and Tale'
    expect { test_error.chain }.to raise_error error_msg
  end
end
