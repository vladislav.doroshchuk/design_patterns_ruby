require 'rspec'
require '../composite'

describe TvSeriesComponent do
  let(:black_mirror) { TvSeries.new('Black Mirror') }
  let(:season_one) { Season.new('S1') }
  let(:season_two) { Season.new('S2') }

  specify 'should add series to season and seasons to serial and output them' do
    season_one.add_child Episode.new('The National Anthem')
    season_one.add_child Episode.new('Fifteen Million Merits')
    black_mirror.add_child season_one
    expect(black_mirror.tv_series).to eq('Black Mirror')
    expect(black_mirror.children[0].title).to eq('S1')
    expect(black_mirror.children[0].children[0].title).to eq('The National Anthem')
    expect(black_mirror.children[0].children[1].title).to eq('Fifteen Million Merits')
  end

  specify 'should return parent season and parent serial of series' do
    season_two.add_child Episode.new('Be Right Back')
    season_two.add_child Episode.new('White Bear')
    black_mirror.add_child season_two
    expect(black_mirror.children[0].title).to eq('S2')
    expect(black_mirror.children[0].children[0].title).to eq('Be Right Back')
    expect(black_mirror.children[0].children[1].title).to eq('White Bear')
    black_mirror_s02_e01 = black_mirror.children[0].children[1]
    expect(black_mirror_s02_e01.parent.title).to eq('S2')
    expect(black_mirror_s02_e01.parent.parent.tv_series).to eq('Black Mirror')
  end
end
