require 'rspec'
require '../observer'

describe Spectator do
  let(:cinema) { Cinema.new }

  specify 'add spectators and inform them' do
    spectators = %w[Jon Jessy Tom]
    spectators.each do |name|
      cinema.add_spectator(Spectator.new(name))
    end
    check_notify cinema, spectators, 3
    cinema.remove_spectator(0)
    check_notify cinema, %w[Jessy Tom], 2
  end

  def check_notify(cinema, spectators, number)
    cinema.movie = 'It'
    expect(cinema.notify.size).to eq(number)
    cinema.notify.each_with_index do |value, index|
      msg = "Spectator #{spectators[index]} is informed about new movie: It"
      expect(value).to eq(msg)
    end
  end
end
