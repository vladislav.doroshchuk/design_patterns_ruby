require 'rspec'
require '../facade'

describe DeliverFacade do
  let(:facebook) { FacebookMessage.new }
  let(:email) { Email.new }
  let(:telegram) { Telegram.new }
  let(:crow) { CrowsDeliveryCompany.new }

  specify 'should deliver first part of messages' do
    deliver_facade = DeliverFacade.new(facebook, email, telegram, crow)
    deliver_part_one = deliver_facade.deliver_part_one(
      'Sam',
      'The Winds of Winter',
      'Winter is coming',
      'test@te.com',
      123
    )
    expect(deliver_part_one.size).to eq(3)
    message = 'User Sam post in Facebook the message "The Winds of Winter"'
    expect(deliver_part_one[0]).to eq(message)
    message = 'User Sam send to address test@te.com email "The Winds of Winter"'
    expect(deliver_part_one[1]).to eq(message)
    message = 'User Sam write down "Winter is coming" in Telegram in chat 123'
    expect(deliver_part_one[2]).to eq(message)
  end

  specify 'should deliver second part of messages' do
    deliver_facade = DeliverFacade.new(facebook, email, telegram, crow)
    deliver_part_two = deliver_facade.deliver_part_two(
      'Sam',
      'The wall was destroyed',
      'The White Walkers came',
      'John Snow',
      'all'
    )
    expect(deliver_part_two.size).to eq(2)
    message = 'User Sam post in Facebook the message "The wall was destroyed"'
    expect(deliver_part_two[0]).to eq(message)
    message = 'Send letter "The White Walkers came" from lord John Snow to all'
    expect(deliver_part_two[1]).to eq(message)
  end
end
