require 'rspec'
require '../flyweight'

describe VideoCollection do
  let(:video_collection) { VideoCollection.new }

  specify 'should deliver first part of messages' do
    video_collection.add_movie('Avengers: Infinity War', 'Fantasy', 2018, 'USA')
    video_collection.add_movie('Black Panther', 'Fantasy', 2018, 'USA')
    video_collection.add_movie('Tomb Raider', 'Fantasy/Action', 2018, 'USA')
    movies = video_collection.all_movies
    expect(movies.size).to eq(3)
    expect(movies[0]['name']).to eq('Avengers: Infinity War')
    expect(movies[1]['name']).to eq('Black Panther')
    expect(movies[2]['name']).to eq('Tomb Raider')

    expect(movies[0]['genre']).to eq('Fantasy')
    expect(movies[1]['genre']).to eq('Fantasy')
    expect(movies[2]['genre']).to eq('Fantasy/Action')

    expect(movies[0]['movie_option']).to eq(movies[1]['movie_option'])
    expect(movies[0]['movie_option']).not_to eq(movies[2]['movie_option'])
  end
end
