require 'rspec'
require '../memento'

describe History do
  let(:browser) { Browser.new }
  let(:history) { History.new }
  let(:links) do
    %w[https://stackoverflow.com https://github.com https://slack.com https://slack.com]
  end
  let(:link) { 'https://github.com' }
  let(:link3) { 'https://www.google.com' }
  let(:link4) { 'https://slack.com' }

  specify 'should return movie info' do
    add_links
    expect(browser.link).to eq(links[3])
    expect(browser.link_from_history(history.undo)).to eq(links[2])
    expect(browser.link_from_history(history.undo)).to eq(links[1])
    first_link = browser.link_from_history(history.history_by_index(0))
    expect(first_link).to eq(links[0])
    last_link = browser.link_from_history(history.history_by_index(3))
    expect(last_link).to eq(links[3])
  end

  def add_links
    links.each do |link|
      browser.follow_the_link(link)
      history.add(browser.save_link_to_history)
    end
  end
end
