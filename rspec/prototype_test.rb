require 'rspec'
require '../prototype'

describe MoviePrototype do
  let(:comedy_movie_prototype) { ComedyMoviePrototype.new }
  let(:horror_movie_prototype) { HorrorMoviePrototype.new }

  specify 'should create comedy movie "Terminal"' do
    comedy_movie = comedy_movie_prototype.clone
    comedy_movie.title = 'Terminal'
    expect(comedy_movie.genre).to eq('Comedy')
    expect(comedy_movie.title).to eq('Terminal')
  end

  specify 'should create horror movie "It"' do
    comedy_movie = horror_movie_prototype.clone
    comedy_movie.title = 'It'
    expect(comedy_movie.genre).to eq('Horror')
    expect(comedy_movie.title).to eq('It')
  end
end
