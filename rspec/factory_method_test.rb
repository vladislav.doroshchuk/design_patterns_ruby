require 'rspec'
require '../factory_method.rb'

describe Button do
  let(:button) { Button.new('red') }
  specify 'sutton color should be red' do
    expect(button.color).to eq('red')
    button_green = Button.new('green')
    expect(button_green.color).to eq('green')
  end
end

describe ClientApplication do
  specify 'should render red Windows Button' do
    app = ClientApplication.new('windows')
    button = app.main('red')
    expect(button).to be_an_instance_of(WindowsButton)
    expect(button.color).to eq('red')
    expect(button.type).to eq('Windows')
  end

  specify 'should render blue HTML Button' do
    app_html = ClientApplication.new('html')
    button_html = app_html.main('blue')
    expect(button_html).to be_an_instance_of(HTMLButton)
    expect(button_html.color).to eq('blue')
    expect(button_html.type).to eq('HTML')
  end

  specify 'raises an error if unknown operating system' do
    error_msg = 'Error! Unknown operating system!'
    expect { ClientApplication.new('test') }.to raise_error error_msg
  end
end
