require 'rspec'
require '../proxy'

describe TheaterProxy do
  let(:movie) { Movie.new("It", 18)}
  let(:client_bill) { Client.new("Bill", 15)}
  let(:client_sam) { Client.new("Sam", 20)}

  specify 'should sell tickets Bill and Sam' do
    theater = Theater.new(movie, client_bill)
    expect(theater.sell_a_ticket).to eq('Client Bill bought a ticket on movie It')
    theater = Theater.new(movie, client_sam)
    expect(theater.sell_a_ticket).to eq('Client Sam bought a ticket on movie It')
  end

  specify 'should sell ticket only to Sam' do
    theater = TheaterProxy.new(movie, client_bill)
    expect(theater.sell_a_ticket).to eq('Buying a ticket is forbidden (restriction on age)')
    theater = TheaterProxy.new(movie, client_sam)
    expect(theater.sell_a_ticket).to eq('Client Sam bought a ticket on movie It')
  end
end
