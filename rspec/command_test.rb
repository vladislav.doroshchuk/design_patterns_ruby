require 'rspec'
require '../command'

describe Operation do
  let(:user) { User.new }
  let(:calculator) { Calc.new }

  specify 'should calculate with possibility of undo' do
    operation = Operation.new(calculator, '+', 10)
    user.calculate_it(operation)
    expect(calculator.result).to eq(10)
    operation = Operation.new(calculator, '*', 5)
    user.calculate_it(operation)
    expect(calculator.result).to eq(50)
    operation = Operation.new(calculator, '-', 10)
    user.calculate_it(operation)
    expect(calculator.result).to eq(40)
    operation = Operation.new(calculator, '/', 4)
    user.calculate_it(operation)
    expect(calculator.result).to eq(10)
    user.undo_calculate
    expect(calculator.result).to eq(40)
    user.undo_calculate
    expect(calculator.result).to eq(50)
  end

  specify 'raises an error if unknown command' do
    operation = Operation.new(calculator, '%', 10)
    error_msg = 'Error! Unknown command!'
    expect { user.calculate_it(operation) }.to raise_error error_msg
  end
end
