require 'rspec'
require '../strategy'

describe SortStrategy do
  let(:array_example) { [3, 44, 38, 5, 47, 15, 36, 26] }
  let(:sort_result) { '3,5,15,26,36,38,44,47' }

  specify 'should sort using Insertion sorting algorithm' do
    strategy_insertion = Insertion.new(array_example)
    sort_strategy = SortStrategy.new(strategy_insertion)
    expect(sort_strategy.sort).to eq("Class Insertion result :#{sort_result}")
  end

  specify 'should sort using Bubble sorting algorithm' do
    strategy_bubble = Bubble.new(array_example)
    sort_strategy = SortStrategy.new(strategy_bubble)
    expect(sort_strategy.sort).to eq("Class Bubble result :#{sort_result}")
  end

  specify 'should sort using Counting sorting algorithm' do
    strategy_counting = Counting.new(array_example)
    sort_strategy = SortStrategy.new(strategy_counting)
    expect(sort_strategy.sort).to eq("Class Counting result :#{sort_result}")
  end
end
