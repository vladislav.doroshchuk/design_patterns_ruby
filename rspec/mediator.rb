require 'rspec'
require '../mediator'

describe Movie do
  let(:avengers) { Movie.new('Avengers: Infinity War', 12) }
  let(:movie_it) { Movie.new('It', 18) }
  let(:client_bill) { Client.new('Bill', 10) }
  let(:client_andry) { Client.new('Andry', 16) }
  let(:client_john) { Client.new('John', 25) }

  specify 'should return age limit for every genre' do
    cinema_box_office = CinemaBoxOffice.new('Portal', client_bill, avengers)
    msg = 'Buying a ticket to Bill is forbidden (restriction on age)'
    expect(cinema_box_office.buy_a_ticket).to eq(msg)

    cinema_box_office = CinemaBoxOffice.new('Portal', client_andry, avengers)
    msg = 'Client Andry bought a ticket on movie Avengers: Infinity War'
    expect(cinema_box_office.buy_a_ticket).to eq(msg)

    cinema_box_office = CinemaBoxOffice.new('Portal', client_john, avengers)
    msg = 'Client John bought a ticket on movie Avengers: Infinity War'
    expect(cinema_box_office.buy_a_ticket).to eq(msg)

    cinema_box_office = CinemaBoxOffice.new('Portal', client_bill, movie_it)
    msg = 'Buying a ticket to Bill is forbidden (restriction on age)'
    expect(cinema_box_office.buy_a_ticket).to eq(msg)

    cinema_box_office = CinemaBoxOffice.new('Portal', client_andry, movie_it)
    msg = 'Buying a ticket to Andry is forbidden (restriction on age)'
    expect(cinema_box_office.buy_a_ticket).to eq(msg)

    cinema_box_office = CinemaBoxOffice.new('Portal', client_john, movie_it)
    msg = 'Client John bought a ticket on movie It'
    expect(cinema_box_office.buy_a_ticket).to eq(msg)
  end
end
