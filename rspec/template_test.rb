require 'rspec'
require '../template'

describe DialogTemplate do
  let(:windows_dialog) { WindowsButtonDialog.new }
  let(:html_dialog) { HTMLButtonDialog.new }
  let(:template_dialog) { DialogTemplate.new }
  let(:dialog_widows_msg) { 'Dialog Windows Button Information' }
  let(:dialog_html_msg) { 'Dialog HTML Button Information' }

  specify 'should return dialogs with needed button' do
    expect(windows_dialog.generate_dialog!).to eq(dialog_widows_msg)
    expect(html_dialog.generate_dialog!).to eq(dialog_html_msg)
  end

  specify 'raises an error if use template dialog' do
    error_msg = 'Error! Not implemented.'
    expect { template_dialog.generate_dialog! }.to raise_error error_msg
  end
end
