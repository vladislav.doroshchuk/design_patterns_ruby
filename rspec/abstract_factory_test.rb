require 'rspec'
require '../abstract_factory.rb'

describe ApplicationConfigurator do
  specify 'should render Windows Button and CheckBox' do
    app_config = ApplicationConfigurator.new('windows')
    app = app_config.main
    app.create_ui
    expect(app.render).to eq('button: WinButton, checkbox: WinCheckbox')
  end

  specify 'should render Mac Button and CheckBox' do
    app_config = ApplicationConfigurator.new('mac')
    app = app_config.main
    app.create_ui
    expect(app.render).to eq('button: MacButton, checkbox: MacCheckbox')
  end

  specify 'raises an error if unknown operating system' do
    error_msg = 'Error! Unknown operating system!'
    expect { ApplicationConfigurator.new('test') }.to raise_error error_msg
  end
end
