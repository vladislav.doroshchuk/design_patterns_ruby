require 'rspec'
require '../decorator'

describe AuthorDecorator do
  let(:author) { Author.new('Name', 'Surname', 100, '123-45-67') }

  specify 'should expand the Author with full_name and full_phone' do
    author_decorator = AuthorDecorator.new(author)
    expect(author_decorator.full_name).to eq('Name Surname')
    expect(author_decorator.full_phone).to eq('+(100)123-45-67')
  end
end
