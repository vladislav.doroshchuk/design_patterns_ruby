require 'rspec'
require '../visitor'

describe Journey do
  let(:temple) { Temple.new('Temple') }
  let(:stadium) { Stadium.new('Stadium') }
  let(:theater) { Theater.new('Theater') }
  let(:journey) { Journey.new }
  let(:sights_visitor) { SightsVisitor.new }
  let(:sights_photo_visitor) { SightsPhotoVisitor.new }
  let(:sights_visitor_msg) do
    [
      'Visiting: Temple and prays there',
      'Visiting: Stadium and cheer there',
      'Visiting: Theater and applaud there'
    ]
  end
  let(:sights_photo_visitor_msg) do
    [
      'Visiting with camera: Temple and prays there',
      'Visiting with camera: Stadium and cheer there',
      'Visiting with camera: Theater and applaud there'
    ]
  end

  specify 'should visit all sights' do
    add_sights
    check_msg sights_visitor, sights_visitor_msg
  end

  specify 'should visit all sights with a camera' do
    add_sights
    check_msg sights_photo_visitor, sights_photo_visitor_msg
  end

  def check_msg(sights, sights_msg)
    sights_msg.each do |msg|
      STDOUT.should_receive(:puts).with(msg)
    end
    journey.visit(sights)
  end

  def add_sights
    journey.add_sight(temple)
    journey.add_sight(stadium)
    journey.add_sight(theater)
  end
end
