require 'rspec'
require '../state'

describe PhoneTurnedOff do
  let(:phone) { PhoneTurnedOff.new }
  let(:msg) do
    [
      'Phone is turned off',
      'Phone is turned on',
      'Phone require click to start button',
      'Phone is ready to work',
      'Phone is turned off'
    ]
  end

  specify 'should change states of phone' do
    5.times do |index|
      expect(phone.status.status_msg).to eq(msg[index])
      phone.next_status
    end
  end
end
