require 'rspec'
require '../adapter'

describe ScrewdriverAdapter do
  let(:screwdriver) { Screwdriver.new }

  specify 'should create only one Settings instance' do
    screwdriver = Screwdriver.new
    screwdriver_adapter = ScrewdriverAdapter.new(screwdriver)
    tool = Tool.new(screwdriver_adapter)
    expect(tool.use_tool).to eq('Screwdriver unscrewed the screw')
  end
end
