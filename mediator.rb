# Movie
class Movie
  attr_accessor :title, :age_limit
  def initialize(title, age_limit)
    @title = title
    @age_limit = age_limit
  end
end

# Client
class Client
  attr_accessor :name, :age
  def initialize(name, age)
    @name = name
    @age = age
  end
end

# Cinema Box Office implement the Mediator pattern
class CinemaBoxOffice
  attr_accessor :cinema_title, :client, :movie
  def initialize(cinema_title, client, movie)
    @cinema_title = cinema_title
    @client = client
    @movie = movie
  end

  def buy_a_ticket
    if movie.age_limit <= client.age
      return "Client #{client.name} bought a ticket on movie #{movie.title}"
    end
    "Buying a ticket to #{client.name} is forbidden (restriction on age)"
  end
end

movie_avengers = Movie.new('Avengers: Infinity War', 12)
movie_it = Movie.new('It', 18)

client_bill = Client.new('Bill', 10)
client_andry = Client.new('Andry', 16)
client_john = Client.new('John', 25)

cinema_box_office = CinemaBoxOffice.new('Portal', client_bill, movie_avengers)
puts cinema_box_office.buy_a_ticket

cinema_box_office = CinemaBoxOffice.new('Portal', client_andry, movie_avengers)
puts cinema_box_office.buy_a_ticket

cinema_box_office = CinemaBoxOffice.new('Portal', client_john, movie_avengers)
puts cinema_box_office.buy_a_ticket

cinema_box_office = CinemaBoxOffice.new('Portal', client_bill, movie_it)
puts cinema_box_office.buy_a_ticket

cinema_box_office = CinemaBoxOffice.new('Portal', client_andry, movie_it)
puts cinema_box_office.buy_a_ticket

cinema_box_office = CinemaBoxOffice.new('Portal', client_john, movie_it)
puts cinema_box_office.buy_a_ticket
