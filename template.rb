# Dialog Template
class DialogTemplate
  def generate_dialog!
    "#{title_dialog} #{render_button} #{information}"
  end

  private

  def title_dialog
    'Dialog'
  end

  def render_button
    raise('Error! Not implemented.')
  end

  def information
    'Information'
  end
end

# HTML Button
class HTMLButtonDialog < DialogTemplate
  def render_button
    'HTML Button'
  end
end

# Windows Button
class WindowsButtonDialog < DialogTemplate
  def render_button
    'Windows Button'
  end
end

windows_dialod = HTMLButtonDialog.new
puts windows_dialod.generate_dialog!

html_dialod = WindowsButtonDialog.new
puts html_dialod.generate_dialog!
