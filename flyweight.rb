# Movie Options
class MovieOptions
  attr_accessor :genre, :year, :country
  def initialize(genre, year, country)
    self.genre = genre
    self.year = year
    self.country = country
  end
end

# Movie Options Factory (implement Flyweight pattern)
class MovieOptionsFactory
  attr_accessor :movie_options_arr
  def initialize
    self.movie_options_arr = []
  end

  def find_movie_option(genre, year, country)
    movie_options_arr.each do |movie_options|
      if  movie_options && movie_options.genre == genre &&
          movie_options.year == year &&
          movie_options.country == country
        return movie_options
      end
    end
    movie_option = MovieOptions.new(genre, year, country)
    movie_options_arr.push(movie_option)
    movie_option
  end
end

# Movie
class Movie
  attr_accessor :name, :movie_option
  def initialize(name, movie_option)
    self.name = name
    self.movie_option = movie_option
  end

  def movies
    {
      'name' => name,
      'genre' => movie_option.genre,
      'year' => movie_option.year,
      'country' => movie_option.country,
      'movie_option' => movie_option
    }
  end
end

# Video Collection
class VideoCollection
  attr_accessor :movies, :movie_option_factory
  def initialize
    self.movies = []
    self.movie_option_factory = MovieOptionsFactory.new
  end

  def add_movie(name, genre, year, country)
    movie_option = movie_option_factory.find_movie_option(genre, year, country)
    movie = Movie.new(name, movie_option)
    movies.push(movie)
  end

  def all_movies
    movies.map(&:movies)
  end
end

video_collection = VideoCollection.new
video_collection.add_movie(
  'Avengers: Infinity War',
  'Fantasy/Science fiction film',
  2018,
  'USA'
)
video_collection.add_movie(
  'Black Panther',
  'Fantasy/Science fiction film',
  2018,
  'USA'
)
video_collection.add_movie(
  'Tomb Raider',
  'Fantasy/Action',
  2018,
  'USA'
)
video_collection.add_movie(
  'Kill Bill: Volume 1',
  'Crime film/Thriller',
  2003,
  'USA'
)
video_collection.add_movie(
  'Kill Bill: Volume 2',
  'Crime film/Thriller',
  2004,
  'country'
)
puts video_collection.all_movies
